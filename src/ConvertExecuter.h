#ifndef _CONVERTEXECUTER_H_
#define _CONVERTEXECUTER_H_

#include <wx/wx.h>

#include "Executer.h"

class ConvertExecuter : public Executer
{
	private:
		wxString cmd;
		virtual wxString *getCmd();

	public:
		ConvertExecuter();

		long convert(wxString from, wxString to, wxTextCtrl *log);
};

#endif
