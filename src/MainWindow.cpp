#include <wx/wx.h>
#include <wx/clipbrd.h>
#include <wx/aboutdlg.h>
#include <wx/msgdlg.h>

#include "MainWindow.h"
#include "ConvertExecuter.h"
#include "UnpaperExecuter.h"
#include "Options.h"
#include "FileUtils.h"

using namespace std;

MainWindow::MainWindow( wxWindow* parent )
:
UnpaperGUIBase( parent )
{
	this->beforeImage = NULL;
	this->afterImage = NULL;

	// scrolling
	beforeScrolledWindow->Connect( wxEVT_SCROLLWIN_THUMBRELEASE,
		wxCommandEventHandler( MainWindow::beforeScrollHandler ), NULL, this );

	after1ScrolledWindow->Connect( wxEVT_SCROLLWIN_THUMBRELEASE,
		wxCommandEventHandler( MainWindow::after1ScrollHandler ), NULL, this );

	after2ScrolledWindow->Connect( wxEVT_SCROLLWIN_THUMBRELEASE,
		wxCommandEventHandler( MainWindow::after2ScrollHandler ), NULL, this );
}

MainWindow::~MainWindow()
{
	beforeScrolledWindow->Disconnect( wxEVT_SCROLLWIN_THUMBRELEASE,
		wxCommandEventHandler( MainWindow::beforeScrollHandler ), NULL, this );

	after1ScrolledWindow->Disconnect( wxEVT_SCROLLWIN_THUMBRELEASE,
		wxCommandEventHandler( MainWindow::after1ScrollHandler ), NULL, this );

	after2ScrolledWindow->Disconnect( wxEVT_SCROLLWIN_THUMBRELEASE,
		wxCommandEventHandler( MainWindow::after2ScrollHandler ), NULL, this );
}

long MainWindow::loadImage( wxString fileName, wxImage **image, wxStaticBitmap *bitmap)
{
	if (*image)
		delete *image;

	wxLogNull logNo; // trick to disable warning on opening pbm -> convert will try to do the job

	*image = new wxImage();
	if (! (*image)->LoadFile(fileName))
	{
			// try convert

			ConvertExecuter *converter = new ConvertExecuter();
			wxString tmpFile = FileUtils::getTmpFileName(wxT(".pnm"));

			if ( converter->convert(fileName, tmpFile, logTextCtrl)
				|| ! ((*image)->LoadFile(tmpFile)) )
			{
				// no hope
				wxMessageBox(wxT("Cannot load/convert file"), wxT("File load error"), wxOK | wxICON_ERROR);
				delete converter;
				delete (*image);
				*image = NULL;
				return -1;
			}

			//rm tmp file
			FileUtils::rmFile(FileUtils::quoteName(tmpFile));
			
			delete converter;
	}

	//ok, image loaded
	loadBitmap(*image, bitmap);
	
	return 0;
}

void MainWindow::loadBitmap( wxImage *image, wxStaticBitmap *bitmap)
{
	long scale;
	if ( ! scaleCombo->GetValue().ToLong(&scale) )
	{
		//not an integer
		scale = 100;
		scaleCombo->SetValue(wxT("100"));
	}

	if ((scale > 0) && image && bitmap) {
		wxImage scaledImage = image->Scale(
				image->GetWidth() * scale / 100, image->GetHeight() * scale / 100);

		wxBitmap *bitmapImage = new wxBitmap(scaledImage);

		bitmap->SetBitmap(*bitmapImage);

		delete bitmapImage;
	}
}

void MainWindow::unpaper(int previewNo) // it doesn't check arg! 1 or 2
{
	if (this->currentFile == wxT(""))
		return; //nothing to convert

	wxString tmpFile = FileUtils::getTmpFileName(wxT(".pnm"));

	UnpaperExecuter *unpaperExecuter = new UnpaperExecuter();

	if (! unpaperExecuter->unpaper(this->currentFile, tmpFile, &(this->opts), logTextCtrl))
	{
		// ok
		loadImage(tmpFile, &(this->afterImage), previewNo == 1 ? after1Bitmap : after2Bitmap);
	}

	FileUtils::rmFile(FileUtils::quoteName(tmpFile));

	delete unpaperExecuter;
}

bool MainWindow::getLivePreview(int previewNo)
{
	switch (previewNo) {
		case 1:
			return live1Preview->GetValue();

		case 2:
			return live2Preview->GetValue();
	}

	return false;
}

void MainWindow::finishHandler()
{
	if (getLivePreview(1))
		unpaper(1);

	if (getLivePreview(2))
		unpaper(2);
}

void MainWindow::preview1Handler( wxCommandEvent& event )
{
	storeGUIOpts(); // in not-live mode they may be outdated
	unpaper(1);
}

void MainWindow::preview2Handler( wxCommandEvent& event )
{
	storeGUIOpts(); // in not-live mode they may be outdated
	unpaper(2);
}

void MainWindow::scaleHandler( wxCommandEvent& event )
{
	loadBitmap(beforeImage, beforeBitmap);
	loadBitmap(afterImage, after1Bitmap);
	loadBitmap(afterImage, after2Bitmap);
	m_splitter1->SetSashPosition(m_splitter1->GetSashPosition()); // workaround to enable srollbars if needed
	m_splitter3->SetSashPosition(m_splitter3->GetSashPosition()); // workaround to enable srollbars if needed
}

void MainWindow::beforeScrollHandler( wxCommandEvent& event )
{
	int x, y;

	if (synchroScrolls->GetValue())
	{
		beforeScrolledWindow->GetViewStart(&x, &y);

		after1ScrolledWindow->Scroll(x, y);
		after2ScrolledWindow->Scroll(x, y);
	}
}

void MainWindow::after1ScrollHandler( wxCommandEvent& event )
{
	int x, y;

	if (synchroScrolls->GetValue())
	{
		after1ScrolledWindow->GetViewStart(&x, &y);

		beforeScrolledWindow->Scroll(x, y);
		after2ScrolledWindow->Scroll(x, y);
	}
}

void MainWindow::after2ScrollHandler( wxCommandEvent& event )
{
	int x, y;

	if (synchroScrolls->GetValue())
	{
		after2ScrolledWindow->GetViewStart(&x, &y);

		beforeScrolledWindow->Scroll(x, y);
		after1ScrolledWindow->Scroll(x, y);
	}
}

wxString MainWindow::getFileFilter()
{
	return _("All supported files (*.pnm;*.pbm;*.pgm;*.ppm)|*.pnm;*.pbm;*.pgm;*.ppm|PNM Files (*.pnm)|*.pnm|PBM Files (*.pbm)|*.pbm|PGM Files (*.pgm)|*.pgm|PPM Files (*.ppm)|*.ppm|All files (*.*)|*.*");
}

void MainWindow::openHandler( wxCommandEvent& event )
{
	wxFileDialog* OpenDialog = new wxFileDialog(
		this, _("Choose a image to load"), wxEmptyString, wxEmptyString, 
		getFileFilter(),
		wxFD_OPEN, wxDefaultPosition);
 
	if (OpenDialog->ShowModal() == wxID_OK) // if the user click "Open" instead of "Cancel"
	{
		scaleCombo->SetValue(wxT("100"));
		if ( ! this->loadImage(OpenDialog->GetPath(), &(this->beforeImage), beforeBitmap) &&
			 ! this->loadImage(OpenDialog->GetPath(), &(this->afterImage), after1Bitmap) &&
			 ! this->loadImage(OpenDialog->GetPath(), &(this->afterImage), after2Bitmap))
		{
			// ok, loaded
			m_splitter1->SetSashPosition(m_splitter1->GetSashPosition()); // workaround to enable srollbars if needed
			this->currentFile = OpenDialog->GetPath();
		}
	}

	delete OpenDialog;
}


void MainWindow::saveHandler( wxCommandEvent& event )
{
	if (this->currentFile == wxT(""))
		return; //nothing to convert

	wxFileDialog* SaveDialog = new wxFileDialog(
		this, _("Choose a file to save"), wxEmptyString, wxEmptyString, 
		getFileFilter(),
		wxFD_SAVE, wxDefaultPosition);
 
	if (SaveDialog->ShowModal() == wxID_OK) // if the user click "Open" instead of "Cancel"
	{
		UnpaperExecuter *unpaperExecuter = new UnpaperExecuter();

		if (! unpaperExecuter->unpaper(this->currentFile, SaveDialog->GetPath(), &(this->opts), logTextCtrl))
		{
			wxMessageBox(_("File successfully saved."), _("File save"), wxOK | wxICON_INFORMATION);
		} else {
			wxMessageBox(_("Error while saving result file. Please see the log."), _("File save"), wxOK | wxICON_ERROR);
		}

		delete unpaperExecuter;
	}
}

void MainWindow::presentOptsInGUI()
{
	wxString optval[5];
	if (this->opts.isDisabled(Options::opt_white_threshold))
		whiteTreshold->ChangeValue(wxT("0.9"));
	else
		whiteTreshold->ChangeValue(this->opts.getOption(Options::opt_white_threshold));

	if (this->opts.isDisabled(Options::Options::opt_blackfilter_intensity))
		blackIntensity->ChangeValue(wxT("20"));
	else
		blackIntensity->ChangeValue(this->opts.getOption(Options::Options::opt_blackfilter_intensity));

	if (this->opts.isDisabled(Options::Options::opt_blackfilter_scan_depth))
	{
		blackScanDepthHorizontal->ChangeValue(wxT("500"));
		blackScanDepthVertical->ChangeValue(wxT("500"));
	} else {
		optval[0] = this->opts.getOption(Options::Options::opt_blackfilter_scan_depth);
		optval[1] = optval[0].BeforeFirst(wxT(','));
		optval[2] = optval[0].AfterFirst(wxT(','));

		blackScanDepthHorizontal->ChangeValue(optval[1]);
		blackScanDepthVertical->ChangeValue(optval[2]);
	}

	if (this->opts.isDisabled(Options::Options::opt_blackfilter_scan_direction))
	{
		blackScanHorizontal->SetValue(true);
		blackScanVertical->SetValue(true);
	} else {
		optval[0] = this->opts.getOption(Options::Options::opt_blackfilter_scan_direction);

		blackScanHorizontal->SetValue(optval[0].Contains(wxT("h")));
		blackScanVertical->SetValue(optval[0].Contains(wxT("v")));
	}

	if (this->opts.isDisabled(Options::Options::opt_blackfilter_scan_size))
	{
		blackScanSizeHorizontal->ChangeValue(wxT("20"));
		blackScanSizeVertical->ChangeValue(wxT("20"));
	} else {
		optval[0] = this->opts.getOption(Options::Options::opt_blackfilter_scan_size);
		optval[1] = optval[0].BeforeFirst(wxT(','));
		optval[2] = optval[0].AfterFirst(wxT(','));

		blackScanSizeHorizontal->ChangeValue(optval[1]);
		blackScanSizeVertical->ChangeValue(optval[2]);
	}

	if (this->opts.isDisabled(Options::Options::opt_blackfilter_scan_step))
	{
		blackScanStepHorizontal->ChangeValue(wxT("5"));
		blackScanStepVertical->ChangeValue(wxT("5"));
	} else {
		optval[0] = this->opts.getOption(Options::Options::opt_blackfilter_scan_step);
		optval[1] = optval[0].BeforeFirst(wxT(','));
		optval[2] = optval[0].AfterFirst(wxT(','));

		blackScanStepHorizontal->ChangeValue(optval[1]);
		blackScanStepVertical->ChangeValue(optval[2]);
	}

	if (this->opts.isDisabled(Options::Options::opt_blackfilter_scan_threshold))
		blackScanThreshold->ChangeValue(wxT("0.95"));
	else
		blackScanThreshold->ChangeValue(this->opts.getOption(Options::Options::opt_blackfilter_scan_threshold));

	if (this->opts.isDisabled(Options::Options::opt_black_threshold))
		blackTreshold->ChangeValue(wxT("0.33"));
	else
		blackTreshold->ChangeValue(this->opts.getOption(Options::Options::opt_black_threshold));

	if (this->opts.isDisabled(Options::Options::opt_blurfilter_intensity))
		blurIntensity->ChangeValue(wxT("0.01"));
	else
		blurIntensity->ChangeValue(this->opts.getOption(Options::Options::opt_blurfilter_intensity));

	if (this->opts.isDisabled(Options::Options::opt_blurfilter_size))
	{
		blurSizeHorizontal->ChangeValue(wxT("100"));
		blurSizeVertical->ChangeValue(wxT("100"));
	} else {
		optval[0] = this->opts.getOption(Options::Options::opt_blurfilter_size);
		optval[1] = optval[0].BeforeFirst(wxT(','));
		optval[2] = optval[0].AfterFirst(wxT(','));

		blurSizeHorizontal->ChangeValue(optval[1]);
		blurSizeVertical->ChangeValue(optval[2]);
	}

	if (this->opts.isDisabled(Options::Options::opt_blurfilter_step))
	{
		blurStepHorizontal->ChangeValue(wxT("50"));
		blurStepVertical->ChangeValue(wxT("50"));
	} else {
		optval[0] = this->opts.getOption(Options::Options::opt_blurfilter_step);
		optval[1] = optval[0].BeforeFirst(wxT(','));
		optval[2] = optval[0].AfterFirst(wxT(','));

		blurStepHorizontal->ChangeValue(optval[1]);
		blurStepVertical->ChangeValue(optval[2]);
	}

	if (this->opts.isDisabled(Options::Options::opt_deskew_scan_depth))
		deskewScanDepth->ChangeValue(wxT("0.5"));
	else
		deskewScanDepth->ChangeValue(this->opts.getOption(Options::Options::opt_deskew_scan_depth));

	if (this->opts.isDisabled(Options::Options::opt_deskew_scan_deviation))
		deskewScanDeviation->ChangeValue(wxT("1.0"));
	else
		deskewScanDeviation->ChangeValue(this->opts.getOption(Options::Options::opt_deskew_scan_deviation));

	if (this->opts.isDisabled(Options::Options::opt_deskew_scan_direction))
	{
		deskewScanLeft->SetValue(true);
		deskewScanRight->SetValue(true);
		deskewScanTop->SetValue(false);
		deskewScanBottom->SetValue(false);
	} else {
		optval[0] = this->opts.getOption(Options::Options::opt_deskew_scan_direction);
		
		deskewScanLeft->SetValue(optval[0].Contains(wxT("left")));
		deskewScanRight->SetValue(optval[0].Contains(wxT("right")));
		deskewScanTop->SetValue(optval[0].Contains(wxT("top")));
		deskewScanBottom->SetValue(optval[0].Contains(wxT("bottom")));
	}

	if (this->opts.isDisabled(Options::Options::opt_deskew_scan_range))
		deskewScanRange->ChangeValue(wxT("5.0"));
	else
		deskewScanRange->ChangeValue(this->opts.getOption(Options::Options::opt_deskew_scan_range));

	if (this->opts.isDisabled(Options::Options::opt_deskew_scan_size))
		deskewScanSize->ChangeValue(wxT("1500"));
	else
		deskewScanSize->ChangeValue(this->opts.getOption(Options::Options::opt_deskew_scan_size));

	if (this->opts.isDisabled(Options::Options::opt_deskew_scan_step))
		deskewScanStep->ChangeValue(wxT("0.1"));
	else
		deskewScanStep->ChangeValue(this->opts.getOption(Options::Options::opt_deskew_scan_step));

	if (this->opts.isDisabled(Options::Options::opt_grayfilter_size))
	{
		graySizeHorizontal->ChangeValue(wxT("50"));
		graySizeVertical->ChangeValue(wxT("50"));
	} else {
		optval[0] = this->opts.getOption(Options::Options::opt_grayfilter_size);
		optval[1] = optval[0].BeforeFirst(wxT(','));
		optval[2] = optval[0].AfterFirst(wxT(','));

		graySizeHorizontal->ChangeValue(optval[1]);
		graySizeVertical->ChangeValue(optval[2]);
	}

	if (this->opts.isDisabled(Options::Options::opt_grayfilter_step))
	{
		grayStepHorizontal->ChangeValue(wxT("20"));
		grayStepVertical->ChangeValue(wxT("20"));
	} else {
		optval[0] = this->opts.getOption(Options::Options::opt_grayfilter_step);
		optval[1] = optval[0].BeforeFirst(wxT(','));
		optval[2] = optval[0].AfterFirst(wxT(','));

		grayStepHorizontal->ChangeValue(optval[1]);
		grayStepVertical->ChangeValue(optval[2]);
	}

	if (this->opts.isDisabled(Options::Options::opt_grayfilter_threshold))
		grayThreshold->ChangeValue(wxT("0.5"));
	else
		grayThreshold->ChangeValue(this->opts.getOption(Options::Options::opt_grayfilter_threshold));

	if (this->opts.isDisabled(Options::Options::opt_layout))
		layoutSingle->SetValue(true);
	else {
		optval[0] = this->opts.getOption(Options::Options::opt_layout);
		
		if (optval[0] == wxT("single"))
			layoutSingle->SetValue(true);

		if (optval[0] == wxT("double"))
			layoutDouble->SetValue(true);

		if (optval[0] == wxT("none"))
			layoutNone->SetValue(true);
	}

	if (this->opts.isDisabled(Options::Options::opt_no_blackfilter))
	{
		blackManual->SetValue(true);
		SetBlackControls(true);
	} else {
		blackDisable->SetValue(true);
		SetBlackControls(false);
	}

	if (this->opts.isDisabled(Options::Options::opt_no_blurfilter))
	{
		blurManual->SetValue(true);
		SetBlurControls(true);
	} else {
		blurDisable->SetValue(true);
		SetBlurControls(false);
	}

	if (this->opts.isDisabled(Options::Options::opt_no_deskew))
	{
		deskewManual->SetValue(true);
		SetDeskewControls(true);
	} else {
		deskewDisable->SetValue(true);
		SetDeskewControls(false);
	}

	if (this->opts.isDisabled(Options::Options::opt_no_grayfilter))
	{
		grayManual->SetValue(true);
		SetGrayControls(true);
	} else {
		grayDisable->SetValue(true);
		SetGrayControls(false);
	}

	if (this->opts.isDisabled(Options::Options::opt_noisefilter_intensity))
		noiseIntensity->ChangeValue(wxT("4"));
	else
		noiseIntensity->ChangeValue(this->opts.getOption(Options::Options::opt_noisefilter_intensity));

	if (this->opts.isDisabled(Options::Options::opt_no_noisefilter))
	{
		noiseManual->SetValue(true);
		noiseIntensity->Enable(true);
	} else {
		noiseDisable->SetValue(true);
		noiseIntensity->Enable(false);
	}

	if (this->opts.isDisabled(Options::Options::opt_post_mirror))
	{
		postMirrorHorizontal->SetValue(false);
		postMirrorVertical->SetValue(false);
	} else {
		optval[0] = this->opts.getOption(Options::Options::opt_post_mirror);

		postMirrorHorizontal->SetValue(optval[0].Contains(wxT("h")));
		postMirrorVertical->SetValue(optval[0].Contains(wxT("v")));
	}

	if (this->opts.isDisabled(Options::Options::opt_post_rotate))
		postRotate->SetSelection(0);
	else {
		optval[0] = this->opts.getOption(Options::Options::opt_post_rotate);

		if (optval[0] == wxT(""))
			postRotate->SetSelection(0);

		if (optval[0] == wxT("90"))
			postRotate->SetSelection(1);

		if (optval[0] == wxT("-90"))
			postRotate->SetSelection(2);
	}

	if (this->opts.isDisabled(Options::Options::opt_post_shift))
	{
		postShiftHorizontal->ChangeValue(wxT("0"));
		postShiftVertical->ChangeValue(wxT("0"));
	} else {
		optval[0] = this->opts.getOption(Options::Options::opt_post_shift);
		optval[1] = optval[0].BeforeFirst(wxT(','));
		optval[2] = optval[0].AfterFirst(wxT(','));

		postShiftHorizontal->ChangeValue(optval[1]);
		postShiftVertical->ChangeValue(optval[2]);
	}

	if (this->opts.isDisabled(Options::Options::opt_post_size))
	{
		postSize->SetSelection(0);
		postLandscapeCheck->SetValue(false);
	} else {
		optval[0] = this->opts.getOption(Options::Options::opt_post_size);

		// "" "legal" "letter" "a3" "a4" "a5"
		if (optval[0].StartsWith(wxT("")))
			postSize->SetSelection(0);

		if (optval[0].StartsWith(wxT("legal")))
			postSize->SetSelection(1);

		if (optval[0].StartsWith(wxT("letter")))
			postSize->SetSelection(2);

		if (optval[0].StartsWith(wxT("a3")))
			postSize->SetSelection(3);

		if (optval[0].StartsWith(wxT("a4")))
			postSize->SetSelection(4);

		if (optval[0].StartsWith(wxT("a5")))
			postSize->SetSelection(5);

		if (optval[0].EndsWith(wxT("landscape")))
			postLandscapeCheck->SetValue(true);
		else
			postLandscapeCheck->SetValue(false);
	}

	if (this->opts.isDisabled(Options::Options::opt_pre_mirror))
	{
		preMirrorHorizontal->SetValue(false);
		preMirrorVertical->SetValue(false);
	} else {
		optval[0] = this->opts.getOption(Options::Options::opt_pre_mirror);

		preMirrorHorizontal->SetValue(optval[0].Contains(wxT("h")));
		preMirrorVertical->SetValue(optval[0].Contains(wxT("v")));
	}

	if (this->opts.isDisabled(Options::Options::opt_pre_rotate))
		preRotate->SetSelection(0);
	else {
		optval[0] = this->opts.getOption(Options::Options::opt_pre_rotate);

		if (optval[0] == wxT(""))
			preRotate->SetSelection(0);

		if (optval[0] == wxT("90"))
			preRotate->SetSelection(1);

		if (optval[0] == wxT("-90"))
			preRotate->SetSelection(2);
	}

	if (this->opts.isDisabled(Options::Options::opt_pre_shift))
	{
		preShiftHorizontal->ChangeValue(wxT("0"));
		preShiftVertical->ChangeValue(wxT("0"));
	} else {
		optval[0] = this->opts.getOption(Options::Options::opt_pre_shift);
		optval[1] = optval[0].BeforeFirst(wxT(','));
		optval[2] = optval[0].AfterFirst(wxT(','));

		preShiftHorizontal->ChangeValue(optval[1]);
		preShiftVertical->ChangeValue(optval[2]);
	}

	if (this->opts.isDisabled(Options::Options::opt_size))
	{
		sizeChoice->SetSelection(0);
		landscapeCheck->SetValue(false);
	} else {
		optval[0] = this->opts.getOption(Options::Options::opt_size);

		// "" "legal" "letter" "a3" "a4" "a5"
		if (optval[0].StartsWith(wxT("")))
			sizeChoice->SetSelection(0);

		if (optval[0].StartsWith(wxT("legal")))
			sizeChoice->SetSelection(1);

		if (optval[0].StartsWith(wxT("letter")))
			sizeChoice->SetSelection(2);

		if (optval[0].StartsWith(wxT("a3")))
			sizeChoice->SetSelection(3);

		if (optval[0].StartsWith(wxT("a4")))
			sizeChoice->SetSelection(4);

		if (optval[0].StartsWith(wxT("a5")))
			sizeChoice->SetSelection(5);

		if (optval[0].EndsWith(wxT("landscape")))
			landscapeCheck->SetValue(true);
		else
			landscapeCheck->SetValue(false);
	}

	if (this->opts.isDisabled(Options::Options::opt_white_threshold))
		whiteTreshold->ChangeValue(wxT("0.9"));
	else
		whiteTreshold->ChangeValue(this->opts.getOption(Options::Options::opt_white_threshold));
}

void MainWindow::storeGUIOpts()
{
	wxCommandEvent ev;

	// for all pages
	generalHandler( ev );
	preprocessHandler( ev );
	postprocessHandler( ev );
	noiseHandler( ev );
	blackHandler( ev );
	blurHandler( ev );
	grayHandler( ev );
	deskewHandler( ev );
}

void MainWindow::loadSettingsHandler( wxCommandEvent& event )
{
	wxFileDialog* OpenDialog = new wxFileDialog(
	this, _("Choose settings to load"), wxEmptyString, wxEmptyString, 
	_("All files (*.*)|*.*"),
	wxFD_OPEN, wxDefaultPosition);
 
	if (OpenDialog->ShowModal() == wxID_OK) // if the user click "Open" instead of "Cancel"
	{
		this->opts.loadFromFile(OpenDialog->GetPath());
		
		presentOptsInGUI();
	}

	delete OpenDialog;
}

void MainWindow::saveSettingsHandler( wxCommandEvent& event )
{
	wxFileDialog* SaveDialog = new wxFileDialog(
	this, _("Choose a file to save settings"), wxEmptyString, wxEmptyString, 
	_("All files (*.*)|*.*"),
	wxFD_SAVE, wxDefaultPosition);
 
	if (SaveDialog->ShowModal() == wxID_OK) // if the user click "Open" instead of "Cancel"
	{
		this->opts.saveToFile(SaveDialog->GetPath());
	}

	delete SaveDialog;
}

void MainWindow::copyArgsToClipboardHandler( wxCommandEvent& event )
{
	wxString options = wxT("unpaper input.pnm output.pnm");

	options.Append(this->opts.render());

	if (wxTheClipboard->Open())
  {
    // This data objects are held by the clipboard, 
    // so do not delete them in the app.
    wxTheClipboard->SetData( new wxTextDataObject(options) );
    wxTheClipboard->Close();
  }
}

void MainWindow::aboutHandler( wxCommandEvent& event )
{
	wxAboutDialogInfo info;
	info.SetName(_("unpaper GUI"));
	info.SetVersion(_("1.0"));
	info.SetDescription(_("Graphical User Interface for unpaper by Jens Gulden"));
	info.SetCopyright(_T("(C) 2008-2012 Grzegorz Chimosz"));
	info.SetWebSite(_("https://bitbucket.org/chimi/unpaper-gui"));
	info.SetLicence(_("GNU General Public License Version 2 or later"));
	info.AddDeveloper(_("Grzegorz Chimosz"));
	//AddDocWriter

	wxAboutBox(info);

}

void MainWindow::clearLogHandler( wxCommandEvent& event )
{
	logTextCtrl->Clear();
}

void MainWindow::generalHandler( wxCommandEvent& event )
{
	this->opts.setOption(Options::opt_white_threshold, whiteTreshold->GetValue());
	this->opts.setOption(Options::opt_black_threshold, blackTreshold->GetValue());

	if (layoutSingle->GetValue())
		this->opts.setOption(Options::opt_layout, wxT("single"));
	if (layoutDouble->GetValue())
		this->opts.setOption(Options::opt_layout, wxT("double"));
	if (layoutNone->GetValue())
		this->opts.setOption(Options::opt_layout, wxT("none"));

	finishHandler();
}

void MainWindow::preprocessHandler( wxCommandEvent& event )
{
	wxString optval;

	if (preRotate->GetStringSelection() != wxT(""))
		this->opts.setOption(Options::opt_pre_rotate, preRotate->GetStringSelection());
	else
		this->opts.disableOption(Options::opt_pre_rotate);

	if (preMirrorVertical->GetValue())
		if (preMirrorHorizontal->GetValue())
			optval = wxT("v,h");
		else
			optval = wxT("v");
	else
		if (preMirrorHorizontal->GetValue())
			optval = wxT("h");
		else
			optval = wxT("");

	if (optval != wxT(""))
		this->opts.setOption(Options::opt_pre_mirror, optval);
	else
		this->opts.disableOption(Options::opt_pre_mirror);

	this->opts.setOption(Options::opt_pre_shift, preShiftHorizontal->GetValue() + wxT(",") + preShiftVertical->GetValue());

	if (sizeChoice->GetStringSelection() != wxT(""))
		this->opts.setOption(Options::opt_size, sizeChoice->GetStringSelection()
			+ (landscapeCheck->GetValue() ? wxT("-landscape") : wxT("")));
	else
		this->opts.disableOption(Options::opt_size);

	finishHandler();
}

void MainWindow::postprocessHandler( wxCommandEvent& event )
{
	wxString optval;

	if (postRotate->GetStringSelection() != wxT(""))
		this->opts.setOption(Options::opt_post_rotate, postRotate->GetStringSelection());
	else
		this->opts.disableOption(Options::opt_post_rotate);

	if (postMirrorVertical->GetValue())
		if (postMirrorHorizontal->GetValue())
			optval = wxT("v,h");
		else
			optval = wxT("v");
	else
		if (postMirrorHorizontal->GetValue())
			optval = wxT("h");
		else
			optval = wxT("");

	if (optval != wxT(""))
		this->opts.setOption(Options::opt_post_mirror, optval);
	else
		this->opts.disableOption(Options::opt_post_mirror);

	this->opts.setOption(Options::opt_post_shift, postShiftHorizontal->GetValue() + wxT(",") + postShiftVertical->GetValue());

	if (postSize->GetStringSelection() != wxT(""))
		this->opts.setOption(Options::opt_post_size, postSize->GetStringSelection()
			+ (postLandscapeCheck->GetValue() ? wxT("-landscape") : wxT("")));
	else
		this->opts.disableOption(Options::opt_post_size);

	finishHandler();
}

void MainWindow::noiseHandler( wxCommandEvent& event )
{
	if (noiseManual->GetValue())
	{
		noiseIntensity->Enable();

		this->opts.disableOption(Options::opt_no_noisefilter);
		this->opts.setOption(Options::opt_noisefilter_intensity, noiseIntensity->GetValue());
	} else {
		noiseIntensity->Disable();
	
		this->opts.disableOption(Options::opt_noisefilter_intensity);
		this->opts.setOption(Options::opt_no_noisefilter);
	}

	finishHandler();
}

void MainWindow::SetBlackControls( bool enabled )
{
	blackIntensity->Enable(enabled);
	blackScanHorizontal->Enable(enabled);
	blackScanVertical->Enable(enabled);
	blackScanSizeHorizontal->Enable(enabled);
	blackScanSizeVertical->Enable(enabled);
	blackScanDepthHorizontal->Enable(enabled);
	blackScanDepthVertical->Enable(enabled);
	blackScanStepHorizontal->Enable(enabled);
	blackScanStepVertical->Enable(enabled);
	blackScanThreshold->Enable(enabled);
}

void MainWindow::blackHandler( wxCommandEvent& event )
{
	wxString optval;

	if (blackManual->GetValue())
	{
		SetBlackControls(true);
		
		this->opts.disableOption(Options::opt_no_blackfilter);

		// add options
		this->opts.setOption(Options::opt_blackfilter_intensity, blackIntensity->GetValue());

		if (blackScanVertical->GetValue())
			if (blackScanHorizontal->GetValue())
				optval = wxT("v,h");
			else
				optval = wxT("v");
		else
			if (blackScanHorizontal->GetValue())
				optval = wxT("h");
			else
				optval = wxT("");
	
		if (optval != wxT(""))
			this->opts.setOption(Options::opt_blackfilter_scan_direction, optval);
		else
			this->opts.disableOption(Options::opt_blackfilter_scan_direction);

		this->opts.setOption(Options::opt_blackfilter_scan_size, blackScanSizeHorizontal->GetValue()
			+ wxT(",") + blackScanSizeVertical->GetValue());

		this->opts.setOption(Options::opt_blackfilter_scan_depth, blackScanDepthHorizontal->GetValue()
			+ wxT(",") + blackScanDepthVertical->GetValue());

		this->opts.setOption(Options::opt_blackfilter_scan_step, blackScanStepHorizontal->GetValue()
			+ wxT(",") + blackScanStepVertical->GetValue());

		this->opts.setOption(Options::opt_blackfilter_scan_threshold, blackScanThreshold->GetValue());
	} else
	{
		SetBlackControls(false);

		this->opts.disableOption(Options::opt_blackfilter_intensity);
		this->opts.disableOption(Options::opt_blackfilter_scan_direction);
		this->opts.disableOption(Options::opt_blackfilter_scan_size);
		this->opts.disableOption(Options::opt_blackfilter_scan_depth);
		this->opts.disableOption(Options::opt_blackfilter_scan_step);
		this->opts.disableOption(Options::opt_blackfilter_scan_threshold);

		this->opts.setOption(Options::opt_no_blackfilter);
	}

	finishHandler();
}

void MainWindow::SetBlurControls( bool enabled )
{
	blurIntensity->Enable(enabled);
	blurSizeHorizontal->Enable(enabled);
	blurSizeVertical->Enable(enabled);
	blurStepHorizontal->Enable(enabled);
	blurStepVertical->Enable(enabled);
}

void MainWindow::blurHandler( wxCommandEvent& event )
{
	if (blurManual->GetValue())
	{
		SetBlurControls(true);
		
		this->opts.disableOption(Options::opt_no_blurfilter);

		// add options
		this->opts.setOption(Options::opt_blurfilter_intensity, blurIntensity->GetValue());

		this->opts.setOption(Options::opt_blurfilter_size, blurSizeHorizontal->GetValue()
			+ wxT(",") + blurSizeVertical->GetValue());

		this->opts.setOption(Options::opt_blurfilter_step, blurStepHorizontal->GetValue()
			+ wxT(",") + blurStepVertical->GetValue());
	} else
	{
		SetBlurControls(false);

		this->opts.disableOption(Options::opt_blurfilter_intensity);
		this->opts.disableOption(Options::opt_blurfilter_size);
		this->opts.disableOption(Options::opt_blurfilter_step);

		this->opts.setOption(Options::opt_no_blurfilter);
	}

	finishHandler();
}

void MainWindow::SetGrayControls( bool enabled )
{
	graySizeHorizontal->Enable(enabled);
	graySizeVertical->Enable(enabled);
	grayStepHorizontal->Enable(enabled);
	grayStepVertical->Enable(enabled);
	grayThreshold->Enable(enabled);
}

void MainWindow::grayHandler( wxCommandEvent& event )
{
	if (grayManual->GetValue())
	{
		SetGrayControls(true);
		
		this->opts.disableOption(Options::opt_no_grayfilter);

		// add options
		this->opts.setOption(Options::opt_grayfilter_size, graySizeHorizontal->GetValue()
			+ wxT(",") + graySizeVertical->GetValue());

		this->opts.setOption(Options::opt_grayfilter_step, grayStepHorizontal->GetValue()
			+ wxT(",") + grayStepVertical->GetValue());

		this->opts.setOption(Options::opt_grayfilter_threshold, grayThreshold->GetValue());
	} else
	{
		SetGrayControls(false);

		this->opts.disableOption(Options::opt_grayfilter_size);
		this->opts.disableOption(Options::opt_grayfilter_step);
		this->opts.disableOption(Options::opt_grayfilter_threshold);

		this->opts.setOption(Options::opt_no_grayfilter);
	}

	finishHandler();
}

void MainWindow::SetDeskewControls( bool enabled )
{
	deskewScanSize->Enable(enabled);
	deskewScanLeft->Enable(enabled);
	deskewScanRight->Enable(enabled);
	deskewScanTop->Enable(enabled);
	deskewScanBottom->Enable(enabled);
	deskewScanDepth->Enable(enabled);
	deskewScanRange->Enable(enabled);
	deskewScanStep->Enable(enabled);
	deskewScanDeviation->Enable(enabled);
}

void MainWindow::deskewHandler( wxCommandEvent& event )
{
	wxString optval;

	if (deskewManual->GetValue())
	{
		SetDeskewControls(true);
		
		this->opts.disableOption(Options::opt_no_deskew);

		// add options
		this->opts.setOption(Options::opt_deskew_scan_size, deskewScanSize->GetValue());

		if (deskewScanLeft->GetValue())
			optval.Append(wxT(",left"));

		if (deskewScanRight->GetValue())
			optval.Append(wxT(",right"));

		if (deskewScanTop->GetValue())
			optval.Append(wxT(",top"));

		if (deskewScanBottom->GetValue())
			optval.Append(wxT(",bottom"));

		if (optval != wxT(""))
			this->opts.setOption(Options::opt_deskew_scan_direction, optval.AfterFirst(wxT(',')));
		else
			this->opts.disableOption(Options::opt_deskew_scan_direction);

		this->opts.setOption(Options::opt_deskew_scan_depth, deskewScanDepth->GetValue());
		this->opts.setOption(Options::opt_deskew_scan_range, deskewScanRange->GetValue());
		this->opts.setOption(Options::opt_deskew_scan_step, deskewScanStep->GetValue());
		this->opts.setOption(Options::opt_deskew_scan_deviation, deskewScanDeviation->GetValue());
	} else
	{
		SetDeskewControls(false);

		this->opts.disableOption(Options::opt_deskew_scan_size);
		this->opts.disableOption(Options::opt_deskew_scan_direction);
		this->opts.disableOption(Options::opt_deskew_scan_depth);
		this->opts.disableOption(Options::opt_deskew_scan_range);
		this->opts.disableOption(Options::opt_deskew_scan_step);
		this->opts.disableOption(Options::opt_deskew_scan_deviation);

		this->opts.setOption(Options::opt_no_deskew);
	}

	finishHandler();
}


