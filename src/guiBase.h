///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Apr 17 2008)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __guiBase__
#define __guiBase__

#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/statbmp.h>
#include <wx/sizer.h>
#include <wx/scrolwin.h>
#include <wx/panel.h>
#include <wx/checkbox.h>
#include <wx/button.h>
#include <wx/splitter.h>
#include <wx/combobox.h>
#include <wx/textctrl.h>
#include <wx/notebook.h>
#include <wx/radiobut.h>
#include <wx/choice.h>
#include <wx/menu.h>
#include <wx/frame.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class UnpaperGUIBase
///////////////////////////////////////////////////////////////////////////////
class UnpaperGUIBase : public wxFrame 
{
	private:
	
	protected:
		wxNotebook* m_notebook1;
		wxPanel* m_panel1;
		wxSplitterWindow* m_splitter1;
		wxPanel* m_panel6;
		wxStaticText* m_staticText1;
		wxScrolledWindow* beforeScrolledWindow;
		wxStaticBitmap* beforeBitmap;
		wxPanel* m_panel23;
		wxStaticText* m_staticText2;
		wxSplitterWindow* m_splitter3;
		wxPanel* m_panel7;
		wxScrolledWindow* after1ScrolledWindow;
		wxStaticBitmap* after1Bitmap;
		wxCheckBox* live1Preview;
		wxButton* m_button2;
		wxPanel* m_panel25;
		wxScrolledWindow* after2ScrolledWindow;
		wxStaticBitmap* after2Bitmap;
		wxCheckBox* live2Preview;
		wxButton* m_button21;
		wxStaticText* m_staticText3;
		wxComboBox* scaleCombo;
		wxStaticText* m_staticText4;
		wxCheckBox* synchroScrolls;
		wxPanel* m_panel3;
		wxTextCtrl* logTextCtrl;
		wxButton* m_button3;
		wxNotebook* m_notebook2;
		wxPanel* m_panel4;
		wxStaticText* m_staticText61124;
		wxTextCtrl* whiteTreshold;
		wxStaticText* m_staticText611241;
		wxTextCtrl* blackTreshold;
		wxStaticText* m_staticText611242;
		wxRadioButton* layoutSingle;
		wxRadioButton* layoutDouble;
		wxRadioButton* layoutNone;
		wxPanel* m_panel5;
		wxStaticText* m_staticText612;
		wxChoice* preRotate;
		wxStaticText* m_staticText81;
		wxCheckBox* preMirrorHorizontal;
		wxCheckBox* preMirrorVertical;
		wxStaticText* m_staticText921;
		wxTextCtrl* preShiftHorizontal;
		wxStaticText* m_staticText1031;
		wxTextCtrl* preShiftVertical;
		wxStaticText* m_staticText10121;
		wxStaticText* m_staticText9211;
		wxChoice* sizeChoice;
		wxCheckBox* landscapeCheck;
		wxPanel* m_panel9;
		wxStaticText* m_staticText6121;
		wxChoice* postRotate;
		wxStaticText* m_staticText811;
		wxCheckBox* postMirrorHorizontal;
		wxCheckBox* postMirrorVertical;
		wxStaticText* m_staticText9212;
		wxTextCtrl* postShiftHorizontal;
		wxStaticText* m_staticText10311;
		wxTextCtrl* postShiftVertical;
		wxStaticText* m_staticText101211;
		wxStaticText* m_staticText92111;
		wxChoice* postSize;
		wxCheckBox* postLandscapeCheck;
		wxPanel* m_panel11;
		wxPanel* m_panel201;
		wxRadioButton* noiseDisable;
		wxRadioButton* noiseManual;
		wxPanel* m_panel21;
		wxStaticText* m_staticText6;
		wxTextCtrl* noiseIntensity;
		wxPanel* m_panel12;
		wxPanel* m_panel2011;
		wxRadioButton* blackDisable;
		wxRadioButton* blackManual;
		wxPanel* m_panel211;
		wxStaticText* m_staticText61;
		wxTextCtrl* blackIntensity;
		wxStaticText* m_staticText8;
		wxCheckBox* blackScanHorizontal;
		wxCheckBox* blackScanVertical;
		wxStaticText* m_staticText9;
		wxTextCtrl* blackScanSizeHorizontal;
		wxStaticText* m_staticText10;
		wxTextCtrl* blackScanSizeVertical;
		wxStaticText* m_staticText101;
		wxStaticText* m_staticText91;
		wxTextCtrl* blackScanDepthHorizontal;
		wxStaticText* m_staticText102;
		wxTextCtrl* blackScanDepthVertical;
		wxStaticText* m_staticText1011;
		wxStaticText* m_staticText92;
		wxTextCtrl* blackScanStepHorizontal;
		wxStaticText* m_staticText103;
		wxTextCtrl* blackScanStepVertical;
		wxStaticText* m_staticText1012;
		wxStaticText* m_staticText611;
		wxTextCtrl* blackScanThreshold;
		wxPanel* m_panel13;
		wxPanel* m_panel20111;
		wxRadioButton* blurDisable;
		wxRadioButton* blurManual;
		wxPanel* m_panel2111;
		wxStaticText* m_staticText613;
		wxTextCtrl* blurIntensity;
		wxStaticText* m_staticText93;
		wxTextCtrl* blurSizeHorizontal;
		wxStaticText* m_staticText104;
		wxTextCtrl* blurSizeVertical;
		wxStaticText* m_staticText1013;
		wxStaticText* m_staticText922;
		wxTextCtrl* blurStepHorizontal;
		wxStaticText* m_staticText1032;
		wxTextCtrl* blurStepVertical;
		wxStaticText* m_staticText10122;
		wxPanel* m_panel14;
		wxPanel* m_panel20112;
		wxRadioButton* grayDisable;
		wxRadioButton* grayManual;
		wxPanel* m_panel2112;
		wxStaticText* m_staticText94;
		wxTextCtrl* graySizeHorizontal;
		wxStaticText* m_staticText105;
		wxTextCtrl* graySizeVertical;
		wxStaticText* m_staticText1014;
		wxStaticText* m_staticText923;
		wxTextCtrl* grayStepHorizontal;
		wxStaticText* m_staticText1033;
		wxTextCtrl* grayStepVertical;
		wxStaticText* m_staticText10123;
		wxStaticText* m_staticText6111;
		wxTextCtrl* grayThreshold;
		wxPanel* m_panel15;
		wxPanel* m_panel16;
		wxPanel* m_panel20113;
		wxRadioButton* deskewDisable;
		wxRadioButton* deskewManual;
		wxPanel* m_panel2113;
		wxStaticText* m_staticText614;
		wxTextCtrl* deskewScanSize;
		wxStaticText* m_staticText82;
		wxCheckBox* deskewScanLeft;
		wxCheckBox* deskewScanRight;
		wxCheckBox* deskewScanTop;
		wxCheckBox* deskewScanBottom;
		wxStaticText* m_staticText6112;
		wxTextCtrl* deskewScanDepth;
		wxStaticText* m_staticText61121;
		wxTextCtrl* deskewScanRange;
		wxStaticText* m_staticText61122;
		wxTextCtrl* deskewScanStep;
		wxStaticText* m_staticText61123;
		wxTextCtrl* deskewScanDeviation;
		wxPanel* m_panel17;
		wxPanel* m_panel18;
		wxMenuBar* m_menubar1;
		wxMenu* m_menu1;
		wxMenu* m_menu3;
		wxMenu* m_menu2;
		
		// Virtual event handlers, overide them in your derived class
		virtual void preview1Handler( wxCommandEvent& event ){ event.Skip(); }
		virtual void preview2Handler( wxCommandEvent& event ){ event.Skip(); }
		virtual void scaleHandler( wxCommandEvent& event ){ event.Skip(); }
		virtual void clearLogHandler( wxCommandEvent& event ){ event.Skip(); }
		virtual void generalHandler( wxCommandEvent& event ){ event.Skip(); }
		virtual void preprocessHandler( wxCommandEvent& event ){ event.Skip(); }
		virtual void postprocessHandler( wxCommandEvent& event ){ event.Skip(); }
		virtual void noiseHandler( wxCommandEvent& event ){ event.Skip(); }
		virtual void blackHandler( wxCommandEvent& event ){ event.Skip(); }
		virtual void blurHandler( wxCommandEvent& event ){ event.Skip(); }
		virtual void grayHandler( wxCommandEvent& event ){ event.Skip(); }
		virtual void deskewHandler( wxCommandEvent& event ){ event.Skip(); }
		virtual void openHandler( wxCommandEvent& event ){ event.Skip(); }
		virtual void saveHandler( wxCommandEvent& event ){ event.Skip(); }
		virtual void loadSettingsHandler( wxCommandEvent& event ){ event.Skip(); }
		virtual void saveSettingsHandler( wxCommandEvent& event ){ event.Skip(); }
		virtual void copyArgsToClipboardHandler( wxCommandEvent& event ){ event.Skip(); }
		virtual void aboutHandler( wxCommandEvent& event ){ event.Skip(); }
		
	
	public:
		UnpaperGUIBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("unpaper GUI"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 888,600 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );
		~UnpaperGUIBase();
		void m_splitter1OnIdle( wxIdleEvent& )
		{
		m_splitter1->SetSashPosition( 428 );
		m_splitter1->Disconnect( wxEVT_IDLE, wxIdleEventHandler( UnpaperGUIBase::m_splitter1OnIdle ), NULL, this );
		}
		
		void m_splitter3OnIdle( wxIdleEvent& )
		{
		m_splitter3->SetSashPosition( 216 );
		m_splitter3->Disconnect( wxEVT_IDLE, wxIdleEventHandler( UnpaperGUIBase::m_splitter3OnIdle ), NULL, this );
		}
		
	
};

#endif //__guiBase__
