#ifndef _OPTIONS_H_
#define _OPTIONS_H_

#include <map>
#include <wx/wx.h>


class Options
{
	private:
		std::map<wxString, wxString> *opts;
		static const wxString OPT_EXCLUDE;

	public:
		void setOption(wxString name, wxString val);
		void setOption(wxString name);
		void disableOption(wxString name);

		wxString getOption(wxString name);
		bool isDisabled(wxString name);

		wxString render();

		long saveToFile(wxString fileName);
		long loadFromFile(wxString fileName);

		Options();
		~Options();

		// seded from manual
		static const wxString opt_layout;
		static const wxString opt_start_sheet;
		static const wxString opt_end_sheet;
		static const wxString opt_sheet;
		static const wxString opt_exclude;
		static const wxString opt_pre_rotate;
		static const wxString opt_post_rotate;
		static const wxString opt_pre_mirror;
		static const wxString opt_post_mirror;
		static const wxString opt_pre_shift;
		static const wxString opt_post_shift;
		static const wxString opt_pre_wipe;
		static const wxString opt_post_wipe;
		static const wxString opt_pre_border;
		static const wxString opt_post_border;
		static const wxString opt_pre_mask;
		static const wxString opt_size;
		static const wxString opt_post_size;
		static const wxString opt_stretch;
		static const wxString opt_post_stretch;
		static const wxString opt_zoom;
		static const wxString opt_post_zoom;
		static const wxString opt_blackfilter_scan_direction;
		static const wxString opt_blackfilter_scan_size;
		static const wxString opt_blackfilter_scan_depth;
		static const wxString opt_blackfilter_scan_step;
		static const wxString opt_blackfilter_scan_threshold;
		static const wxString opt_blackfilter_scan_exclude;
		static const wxString opt_blackfilter_intensity;
		static const wxString opt_noisefilter_intensity;
		static const wxString opt_blurfilter_size;
		static const wxString opt_blurfilter_step;
		static const wxString opt_blurfilter_intensity;
		static const wxString opt_grayfilter_size;
		static const wxString opt_grayfilter_step;
		static const wxString opt_grayfilter_threshold;
		static const wxString opt_mask_scan_point;
		static const wxString opt_mask;
		static const wxString opt_mask_scan_direction;
		static const wxString opt_mask_scan_size;
		static const wxString opt_mask_scan_depth;
		static const wxString opt_mask_scan_step;
		static const wxString opt_mask_scan_threshold;
		static const wxString opt_mask_scan_minimum;
		static const wxString opt_mask_scan_maximum;
		static const wxString opt_mask_color;
		static const wxString opt_deskew_scan_direction;
		static const wxString opt_deskew_scan_size;
		static const wxString opt_deskew_scan_depth;
		static const wxString opt_deskew_scan_range;
		static const wxString opt_deskew_scan_step;
		static const wxString opt_deskew_scan_deviation;
		static const wxString opt_wipe;
		static const wxString opt_border;
		static const wxString opt_border_scan_direction;
		static const wxString opt_border_scan_size;
		static const wxString opt_border_scan_step;
		static const wxString opt_border_scan_threshold;
		static const wxString opt_border_align;
		static const wxString opt_border_margin;
		static const wxString opt_white_threshold;
		static const wxString opt_black_threshold;
		static const wxString opt_input_pages;
		static const wxString opt_output_pages;
		static const wxString opt_post_rotation;
		static const wxString opt_sheet_size;
		static const wxString opt_sheet_background;
		static const wxString opt_no_blackfilter;
		static const wxString opt_no_noisefilter;
		static const wxString opt_no_blurfilter;
		static const wxString opt_no_grayfilter;
		static const wxString opt_no_mask_scan;
		static const wxString opt_no_mask_center;
		static const wxString opt_no_deskew;
		static const wxString opt_no_wipe;
		static const wxString opt_no_border;
		static const wxString opt_no_border_scan;
		static const wxString opt_no_border_align;
		static const wxString opt_no_processing;
		static const wxString opt_no_xxx;
		static const wxString opt_no_qpixels;
		static const wxString opt_no_multi_pages;
		static const wxString opt_dpi;
		static const wxString opt_type;
		static const wxString opt_depth;
		static const wxString opt_test_only;
		static const wxString opt_verbose;
		static const wxString opt_input_file_sequence;
		static const wxString opt_output_file_sequence;
		static const wxString opt_start_input;
		static const wxString opt_start_output;
		static const wxString opt_insert_blank;
		static const wxString opt_overwrite;
		static const wxString opt_quiet;
		static const wxString opt_time;
		static const wxString opt_version;
};

#endif

