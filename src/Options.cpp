#include <wx/file.h>
#include <wx/ffile.h>
#include <wx/tokenzr.h>

#include "Options.h"

using namespace std;

Options::Options()
{
	this->opts = new map<wxString, wxString>();

	(*this->opts)[opt_layout] = OPT_EXCLUDE;
	(*this->opts)[opt_start_sheet] = OPT_EXCLUDE;
	(*this->opts)[opt_end_sheet] = OPT_EXCLUDE;
	(*this->opts)[opt_sheet] = OPT_EXCLUDE;
	(*this->opts)[opt_exclude] = OPT_EXCLUDE;
	(*this->opts)[opt_pre_rotate] = OPT_EXCLUDE;
	(*this->opts)[opt_post_rotate] = OPT_EXCLUDE;
	(*this->opts)[opt_pre_mirror] = OPT_EXCLUDE;
	(*this->opts)[opt_post_mirror] = OPT_EXCLUDE;
	(*this->opts)[opt_pre_shift] = OPT_EXCLUDE;
	(*this->opts)[opt_post_shift] = OPT_EXCLUDE;
	(*this->opts)[opt_pre_wipe] = OPT_EXCLUDE;
	(*this->opts)[opt_post_wipe] = OPT_EXCLUDE;
	(*this->opts)[opt_pre_border] = OPT_EXCLUDE;
	(*this->opts)[opt_post_border] = OPT_EXCLUDE;
	(*this->opts)[opt_pre_mask] = OPT_EXCLUDE;
	(*this->opts)[opt_size] = OPT_EXCLUDE;
	(*this->opts)[opt_post_size] = OPT_EXCLUDE;
	(*this->opts)[opt_stretch] = OPT_EXCLUDE;
	(*this->opts)[opt_post_stretch] = OPT_EXCLUDE;
	(*this->opts)[opt_zoom] = OPT_EXCLUDE;
	(*this->opts)[opt_post_zoom] = OPT_EXCLUDE;
	(*this->opts)[opt_blackfilter_scan_direction] = OPT_EXCLUDE;
	(*this->opts)[opt_blackfilter_scan_size] = OPT_EXCLUDE;
	(*this->opts)[opt_blackfilter_scan_depth] = OPT_EXCLUDE;
	(*this->opts)[opt_blackfilter_scan_step] = OPT_EXCLUDE;
	(*this->opts)[opt_blackfilter_scan_threshold] = OPT_EXCLUDE;
	(*this->opts)[opt_blackfilter_scan_exclude] = OPT_EXCLUDE;
	(*this->opts)[opt_blackfilter_intensity] = OPT_EXCLUDE;
	(*this->opts)[opt_blurfilter_size] = OPT_EXCLUDE;
	(*this->opts)[opt_blurfilter_step] = OPT_EXCLUDE;
	(*this->opts)[opt_blurfilter_intensity] = OPT_EXCLUDE;
	(*this->opts)[opt_grayfilter_size] = OPT_EXCLUDE;
	(*this->opts)[opt_grayfilter_step] = OPT_EXCLUDE;
	(*this->opts)[opt_grayfilter_threshold] = OPT_EXCLUDE;
	(*this->opts)[opt_mask_scan_point] = OPT_EXCLUDE;
	(*this->opts)[opt_mask] = OPT_EXCLUDE;
	(*this->opts)[opt_mask_scan_direction] = OPT_EXCLUDE;
	(*this->opts)[opt_mask_scan_size] = OPT_EXCLUDE;
	(*this->opts)[opt_mask_scan_depth] = OPT_EXCLUDE;
	(*this->opts)[opt_mask_scan_step] = OPT_EXCLUDE;
	(*this->opts)[opt_mask_scan_threshold] = OPT_EXCLUDE;
	(*this->opts)[opt_mask_scan_minimum] = OPT_EXCLUDE;
	(*this->opts)[opt_mask_scan_maximum] = OPT_EXCLUDE;
	(*this->opts)[opt_layout] = OPT_EXCLUDE;
	(*this->opts)[opt_mask_color] = OPT_EXCLUDE;
	(*this->opts)[opt_deskew_scan_direction] = OPT_EXCLUDE;
	(*this->opts)[opt_deskew_scan_size] = OPT_EXCLUDE;
	(*this->opts)[opt_deskew_scan_depth] = OPT_EXCLUDE;
	(*this->opts)[opt_deskew_scan_range] = OPT_EXCLUDE;
	(*this->opts)[opt_deskew_scan_step] = OPT_EXCLUDE;
	(*this->opts)[opt_deskew_scan_deviation] = OPT_EXCLUDE;
	(*this->opts)[opt_wipe] = OPT_EXCLUDE;
	(*this->opts)[opt_layout] = OPT_EXCLUDE;
	(*this->opts)[opt_border] = OPT_EXCLUDE;
	(*this->opts)[opt_border_scan_direction] = OPT_EXCLUDE;
	(*this->opts)[opt_border_scan_size] = OPT_EXCLUDE;
	(*this->opts)[opt_border_scan_step] = OPT_EXCLUDE;
	(*this->opts)[opt_border_scan_threshold] = OPT_EXCLUDE;
	(*this->opts)[opt_border_align] = OPT_EXCLUDE;
	(*this->opts)[opt_border_margin] = OPT_EXCLUDE;
	(*this->opts)[opt_white_threshold] = OPT_EXCLUDE;
	(*this->opts)[opt_black_threshold] = OPT_EXCLUDE;
	(*this->opts)[opt_input_pages] = OPT_EXCLUDE;
	(*this->opts)[opt_output_pages] = OPT_EXCLUDE;
	(*this->opts)[opt_post_rotation] = OPT_EXCLUDE;
	(*this->opts)[opt_sheet_size] = OPT_EXCLUDE;
	(*this->opts)[opt_sheet_background] = OPT_EXCLUDE;
	(*this->opts)[opt_no_blackfilter] = OPT_EXCLUDE;
	(*this->opts)[opt_no_noisefilter] = OPT_EXCLUDE;
	(*this->opts)[opt_no_blurfilter] = OPT_EXCLUDE;
	(*this->opts)[opt_no_grayfilter] = OPT_EXCLUDE;
	(*this->opts)[opt_no_mask_scan] = OPT_EXCLUDE;
	(*this->opts)[opt_mask] = OPT_EXCLUDE;
	(*this->opts)[opt_no_mask_center] = OPT_EXCLUDE;
	(*this->opts)[opt_layout] = OPT_EXCLUDE;
	(*this->opts)[opt_no_deskew] = OPT_EXCLUDE;
	(*this->opts)[opt_no_wipe] = OPT_EXCLUDE;
	(*this->opts)[opt_wipe] = OPT_EXCLUDE;
	(*this->opts)[opt_no_border] = OPT_EXCLUDE;
	(*this->opts)[opt_border] = OPT_EXCLUDE;
	(*this->opts)[opt_no_border_scan] = OPT_EXCLUDE;
	(*this->opts)[opt_no_border_align] = OPT_EXCLUDE;
	(*this->opts)[opt_no_processing] = OPT_EXCLUDE;
	(*this->opts)[opt_no_xxx] = OPT_EXCLUDE;
	(*this->opts)[opt_no_qpixels] = OPT_EXCLUDE;
	(*this->opts)[opt_no_multi_pages] = OPT_EXCLUDE;
	(*this->opts)[opt_dpi] = OPT_EXCLUDE;
	(*this->opts)[opt_type] = OPT_EXCLUDE;
	(*this->opts)[opt_depth] = OPT_EXCLUDE;
	(*this->opts)[opt_test_only] = OPT_EXCLUDE;
	(*this->opts)[opt_verbose] = OPT_EXCLUDE;
	(*this->opts)[opt_input_file_sequence] = OPT_EXCLUDE;
	(*this->opts)[opt_output_file_sequence] = OPT_EXCLUDE;
	(*this->opts)[opt_start_input] = OPT_EXCLUDE;
	(*this->opts)[opt_start_output] = OPT_EXCLUDE;
	(*this->opts)[opt_insert_blank] = OPT_EXCLUDE;
	(*this->opts)[opt_overwrite] = OPT_EXCLUDE;
	(*this->opts)[opt_quiet] = OPT_EXCLUDE;
	(*this->opts)[opt_verbose] = OPT_EXCLUDE;
	(*this->opts)[opt_time] = OPT_EXCLUDE;
	(*this->opts)[opt_version] = OPT_EXCLUDE;
	(*this->opts)[opt_blackfilter_intensity] = OPT_EXCLUDE;
	(*this->opts)[opt_blackfilter_scan_depth] = OPT_EXCLUDE;
	(*this->opts)[opt_blackfilter_scan_direction] = OPT_EXCLUDE;
	(*this->opts)[opt_blackfilter_scan_exclude] = OPT_EXCLUDE;
	(*this->opts)[opt_blackfilter_scan_size] = OPT_EXCLUDE;
	(*this->opts)[opt_blackfilter_scan_step] = OPT_EXCLUDE;
	(*this->opts)[opt_blackfilter_scan_threshold] = OPT_EXCLUDE;
	(*this->opts)[opt_black_threshold] = OPT_EXCLUDE;
	(*this->opts)[opt_blurfilter_intensity] = OPT_EXCLUDE;
	(*this->opts)[opt_blurfilter_size] = OPT_EXCLUDE;
	(*this->opts)[opt_blurfilter_step] = OPT_EXCLUDE;
	(*this->opts)[opt_border] = OPT_EXCLUDE;
	(*this->opts)[opt_border_align] = OPT_EXCLUDE;
	(*this->opts)[opt_border_margin] = OPT_EXCLUDE;
	(*this->opts)[opt_border_scan_direction] = OPT_EXCLUDE;
	(*this->opts)[opt_border_scan_size] = OPT_EXCLUDE;
	(*this->opts)[opt_border_scan_step] = OPT_EXCLUDE;
	(*this->opts)[opt_border_scan_threshold] = OPT_EXCLUDE;
	(*this->opts)[opt_depth] = OPT_EXCLUDE;
	(*this->opts)[opt_deskew_scan_depth] = OPT_EXCLUDE;
	(*this->opts)[opt_deskew_scan_deviation] = OPT_EXCLUDE;
	(*this->opts)[opt_deskew_scan_direction] = OPT_EXCLUDE;
	(*this->opts)[opt_deskew_scan_range] = OPT_EXCLUDE;
	(*this->opts)[opt_deskew_scan_size] = OPT_EXCLUDE;
	(*this->opts)[opt_deskew_scan_step] = OPT_EXCLUDE;
	(*this->opts)[opt_dpi] = OPT_EXCLUDE;
	(*this->opts)[opt_end_sheet] = OPT_EXCLUDE;
	(*this->opts)[opt_exclude] = OPT_EXCLUDE;
	(*this->opts)[opt_grayfilter_size] = OPT_EXCLUDE;
	(*this->opts)[opt_grayfilter_step] = OPT_EXCLUDE;
	(*this->opts)[opt_grayfilter_threshold] = OPT_EXCLUDE;
	(*this->opts)[opt_input_file_sequence] = OPT_EXCLUDE;
	(*this->opts)[opt_input_pages] = OPT_EXCLUDE;
	(*this->opts)[opt_insert_blank] = OPT_EXCLUDE;
	(*this->opts)[opt_layout] = OPT_EXCLUDE;
	(*this->opts)[opt_mask] = OPT_EXCLUDE;
	(*this->opts)[opt_mask_color] = OPT_EXCLUDE;
	(*this->opts)[opt_mask_scan_depth] = OPT_EXCLUDE;
	(*this->opts)[opt_mask_scan_direction] = OPT_EXCLUDE;
	(*this->opts)[opt_mask_scan_maximum] = OPT_EXCLUDE;
	(*this->opts)[opt_mask_scan_minimum] = OPT_EXCLUDE;
	(*this->opts)[opt_mask_scan_point] = OPT_EXCLUDE;
	(*this->opts)[opt_mask_scan_size] = OPT_EXCLUDE;
	(*this->opts)[opt_mask_scan_step] = OPT_EXCLUDE;
	(*this->opts)[opt_mask_scan_threshold] = OPT_EXCLUDE;
	(*this->opts)[opt_no_blackfilter] = OPT_EXCLUDE;
	(*this->opts)[opt_no_blurfilter] = OPT_EXCLUDE;
	(*this->opts)[opt_no_border] = OPT_EXCLUDE;
	(*this->opts)[opt_no_border_align] = OPT_EXCLUDE;
	(*this->opts)[opt_no_border_scan] = OPT_EXCLUDE;
	(*this->opts)[opt_no_deskew] = OPT_EXCLUDE;
	(*this->opts)[opt_no_grayfilter] = OPT_EXCLUDE;
	(*this->opts)[opt_noisefilter_intensity] = OPT_EXCLUDE;
	(*this->opts)[opt_no_mask_center] = OPT_EXCLUDE;
	(*this->opts)[opt_no_mask_scan] = OPT_EXCLUDE;
	(*this->opts)[opt_no_multi_pages] = OPT_EXCLUDE;
	(*this->opts)[opt_no_noisefilter] = OPT_EXCLUDE;
	(*this->opts)[opt_no_processing] = OPT_EXCLUDE;
	(*this->opts)[opt_no_qpixels] = OPT_EXCLUDE;
	(*this->opts)[opt_no_wipe] = OPT_EXCLUDE;
	(*this->opts)[opt_no_xxx] = OPT_EXCLUDE;
	(*this->opts)[opt_output_file_sequence] = OPT_EXCLUDE;
	(*this->opts)[opt_output_pages] = OPT_EXCLUDE;
	(*this->opts)[opt_overwrite] = OPT_EXCLUDE;
	(*this->opts)[opt_post_border] = OPT_EXCLUDE;
	(*this->opts)[opt_post_mirror] = OPT_EXCLUDE;
	(*this->opts)[opt_post_rotate] = OPT_EXCLUDE;
	(*this->opts)[opt_post_rotation] = OPT_EXCLUDE;
	(*this->opts)[opt_post_shift] = OPT_EXCLUDE;
	(*this->opts)[opt_post_size] = OPT_EXCLUDE;
	(*this->opts)[opt_post_stretch] = OPT_EXCLUDE;
	(*this->opts)[opt_post_wipe] = OPT_EXCLUDE;
	(*this->opts)[opt_post_zoom] = OPT_EXCLUDE;
	(*this->opts)[opt_pre_border] = OPT_EXCLUDE;
	(*this->opts)[opt_pre_mask] = OPT_EXCLUDE;
	(*this->opts)[opt_pre_mirror] = OPT_EXCLUDE;
	(*this->opts)[opt_pre_rotate] = OPT_EXCLUDE;
	(*this->opts)[opt_pre_shift] = OPT_EXCLUDE;
	(*this->opts)[opt_pre_wipe] = OPT_EXCLUDE;
	(*this->opts)[opt_quiet] = OPT_EXCLUDE;
	(*this->opts)[opt_sheet] = OPT_EXCLUDE;
	(*this->opts)[opt_sheet_background] = OPT_EXCLUDE;
	(*this->opts)[opt_sheet_size] = OPT_EXCLUDE;
	(*this->opts)[opt_size] = OPT_EXCLUDE;
	(*this->opts)[opt_start_input] = OPT_EXCLUDE;
	(*this->opts)[opt_start_output] = OPT_EXCLUDE;
	(*this->opts)[opt_start_sheet] = OPT_EXCLUDE;
	(*this->opts)[opt_stretch] = OPT_EXCLUDE;
	(*this->opts)[opt_test_only] = OPT_EXCLUDE;
	(*this->opts)[opt_time] = OPT_EXCLUDE;
	(*this->opts)[opt_type] = OPT_EXCLUDE;
	(*this->opts)[opt_verbose] = OPT_EXCLUDE;
	(*this->opts)[opt_version] = OPT_EXCLUDE;
	(*this->opts)[opt_white_threshold] = OPT_EXCLUDE;
	(*this->opts)[opt_wipe] = OPT_EXCLUDE;
	(*this->opts)[opt_zoom] = OPT_EXCLUDE;
}

Options::~Options()
{
	delete this->opts;
}

void Options::setOption(wxString name, wxString val)
{
	(*this->opts)[name] = val;
}

void Options::setOption(wxString name)
{
	(*this->opts)[name] = wxT("");
}

void Options::disableOption(wxString name)
{
	(*this->opts)[name] = OPT_EXCLUDE;
}

wxString Options::getOption(wxString name)
{
	return (*this->opts)[name];
}

bool Options::isDisabled(wxString name)
{
	return ((*this->opts)[name] == OPT_EXCLUDE);
}

wxString Options::render()
{
	wxString rv;

	map<wxString, wxString>::const_iterator itr;

	for(itr = this->opts->begin(); itr != this->opts->end(); ++itr)
	{
		if (itr->second != OPT_EXCLUDE)
		{
			rv.Append(wxT(" "));
			rv.Append(itr->first);
			rv.Append(wxT(" "));
			rv.Append(itr->second);
		}
		//cout << "Key: " << (*itr).first << " Value: " << (*itr).second;
	}

	return rv;
}

long Options::saveToFile(wxString fileName)
{
	wxFile file;
	wxString settingsLine;

	if (file.Create(fileName, true)) {
		if (! file.Write(wxT("; unpaper-gui settings\n; DON'T EDIT THIS FILE\n\n")))
			return -1;

		map<wxString, wxString>::const_iterator itr;

		for(itr = this->opts->begin(); itr != this->opts->end(); ++itr)
		{
			settingsLine = itr->first;
			settingsLine.Append(wxT("\t"));
			settingsLine.Append(itr->second);
			settingsLine.Append(wxT("\n"));

			if (! file.Write(settingsLine))
				return -1;
		}

		return 0;
	}

	return -1;
}

long Options::loadFromFile(wxString fileName)
{
	wxFFile file;
	wxString settings, line, name, val;
	wxStringTokenizer settingsTokenizer, lineTokenizer;

	if (file.Open(fileName)) {
		if (! file.ReadAll(&settings))
			return -1;

		settingsTokenizer.SetString(settings, wxT("\r\n"));

		while (settingsTokenizer.HasMoreTokens()) {
			line = settingsTokenizer.GetNextToken();

			if (line.Len() && ! line.StartsWith(wxT(";"))) {
				lineTokenizer.SetString(line, wxT("\t\r\n"));
	
				if (lineTokenizer.CountTokens() != 2)
					return -1;
				else {
						name = lineTokenizer.GetNextToken();
						val = lineTokenizer.GetNextToken();
						
						this->setOption(name, val);
						//DEBUG
						//std::cerr << "opt \"" << name.mb_str(wxConvUTF8) << "\" load with value \"" << val.mb_str(wxConvUTF8) << "\"" << endl;
				}
			}
		}

		return 0;
	}

	return -1;
}

const wxString Options::OPT_EXCLUDE = wxT("!");
// seded
const wxString Options::opt_layout = wxT("--layout");
const wxString Options::opt_start_sheet = wxT("--start-sheet");
const wxString Options::opt_end_sheet = wxT("--end-sheet");
const wxString Options::opt_sheet = wxT("--sheet");
const wxString Options::opt_exclude = wxT("--exclude");
const wxString Options::opt_pre_rotate = wxT("--pre-rotate");
const wxString Options::opt_post_rotate = wxT("--post-rotate");
const wxString Options::opt_pre_mirror = wxT("--pre-mirror");
const wxString Options::opt_post_mirror = wxT("--post-mirror");
const wxString Options::opt_pre_shift = wxT("--pre-shift");
const wxString Options::opt_post_shift = wxT("--post-shift");
const wxString Options::opt_pre_wipe = wxT("--pre-wipe");
const wxString Options::opt_post_wipe = wxT("--post-wipe");
const wxString Options::opt_pre_border = wxT("--pre-border");
const wxString Options::opt_post_border = wxT("--post-border");
const wxString Options::opt_pre_mask = wxT("--pre-mask");
const wxString Options::opt_size = wxT("--size");
const wxString Options::opt_post_size = wxT("--post-size");
const wxString Options::opt_stretch = wxT("--stretch");
const wxString Options::opt_post_stretch = wxT("--post-stretch");
const wxString Options::opt_zoom = wxT("--zoom");
const wxString Options::opt_post_zoom = wxT("--post-zoom");
const wxString Options::opt_blackfilter_scan_direction = wxT("--blackfilter-scan-direction");
const wxString Options::opt_blackfilter_scan_size = wxT("--blackfilter-scan-size");
const wxString Options::opt_blackfilter_scan_depth = wxT("--blackfilter-scan-depth");
const wxString Options::opt_blackfilter_scan_step = wxT("--blackfilter-scan-step");
const wxString Options::opt_blackfilter_scan_threshold = wxT("--blackfilter-scan-threshold");
const wxString Options::opt_blackfilter_scan_exclude = wxT("--blackfilter-scan-exclude");
const wxString Options::opt_blackfilter_intensity = wxT("--blackfilter-intensity");
const wxString Options::opt_noisefilter_intensity = wxT("--noisefilter-intensity");
const wxString Options::opt_blurfilter_size = wxT("--blurfilter-size");
const wxString Options::opt_blurfilter_step = wxT("--blurfilter-step");
const wxString Options::opt_blurfilter_intensity = wxT("--blurfilter-intensity");
const wxString Options::opt_grayfilter_size = wxT("--grayfilter-size");
const wxString Options::opt_grayfilter_step = wxT("--grayfilter-step");
const wxString Options::opt_grayfilter_threshold = wxT("--grayfilter-threshold");
const wxString Options::opt_mask_scan_point = wxT("--mask-scan-point");
const wxString Options::opt_mask = wxT("--mask");
const wxString Options::opt_mask_scan_direction = wxT("--mask-scan-direction");
const wxString Options::opt_mask_scan_size = wxT("--mask-scan-size");
const wxString Options::opt_mask_scan_depth = wxT("--mask-scan-depth");
const wxString Options::opt_mask_scan_step = wxT("--mask-scan-step");
const wxString Options::opt_mask_scan_threshold = wxT("--mask-scan-threshold");
const wxString Options::opt_mask_scan_minimum = wxT("--mask-scan-minimum");
const wxString Options::opt_mask_scan_maximum = wxT("--mask-scan-maximum");
const wxString Options::opt_mask_color = wxT("--mask-color");
const wxString Options::opt_deskew_scan_direction = wxT("--deskew-scan-direction");
const wxString Options::opt_deskew_scan_size = wxT("--deskew-scan-size");
const wxString Options::opt_deskew_scan_depth = wxT("--deskew-scan-depth");
const wxString Options::opt_deskew_scan_range = wxT("--deskew-scan-range");
const wxString Options::opt_deskew_scan_step = wxT("--deskew-scan-step");
const wxString Options::opt_deskew_scan_deviation = wxT("--deskew-scan-deviation");
const wxString Options::opt_wipe = wxT("--wipe");
const wxString Options::opt_border = wxT("--border");
const wxString Options::opt_border_scan_direction = wxT("--border-scan-direction");
const wxString Options::opt_border_scan_size = wxT("--border-scan-size");
const wxString Options::opt_border_scan_step = wxT("--border-scan-step");
const wxString Options::opt_border_scan_threshold = wxT("--border-scan-threshold");
const wxString Options::opt_border_align = wxT("--border-align");
const wxString Options::opt_border_margin = wxT("--border-margin");
const wxString Options::opt_white_threshold = wxT("--white-threshold");
const wxString Options::opt_black_threshold = wxT("--black-threshold");
const wxString Options::opt_input_pages = wxT("--input-pages");
const wxString Options::opt_output_pages = wxT("--output-pages");
const wxString Options::opt_post_rotation = wxT("--post-rotation");
const wxString Options::opt_sheet_size = wxT("--sheet-size");
const wxString Options::opt_sheet_background = wxT("--sheet-background");
const wxString Options::opt_no_blackfilter = wxT("--no-blackfilter");
const wxString Options::opt_no_noisefilter = wxT("--no-noisefilter");
const wxString Options::opt_no_blurfilter = wxT("--no-blurfilter");
const wxString Options::opt_no_grayfilter = wxT("--no-grayfilter");
const wxString Options::opt_no_mask_scan = wxT("--no-mask-scan");
const wxString Options::opt_no_mask_center = wxT("--no-mask-center");
const wxString Options::opt_no_deskew = wxT("--no-deskew");
const wxString Options::opt_no_wipe = wxT("--no-wipe");
const wxString Options::opt_no_border = wxT("--no-border");
const wxString Options::opt_no_border_scan = wxT("--no-border-scan");
const wxString Options::opt_no_border_align = wxT("--no-border-align");
const wxString Options::opt_no_processing = wxT("--no-processing");
const wxString Options::opt_no_xxx = wxT("--no-xxx");
const wxString Options::opt_no_qpixels = wxT("--no-qpixels");
const wxString Options::opt_no_multi_pages = wxT("--no-multi-pages");
const wxString Options::opt_dpi = wxT("--dpi");
const wxString Options::opt_type = wxT("--type");
const wxString Options::opt_depth = wxT("--depth");
const wxString Options::opt_test_only = wxT("--test-only");
const wxString Options::opt_verbose = wxT("--verbose");
const wxString Options::opt_input_file_sequence = wxT("--input-file-sequence");
const wxString Options::opt_output_file_sequence = wxT("--output-file-sequence");
const wxString Options::opt_start_input = wxT("--start-input");
const wxString Options::opt_start_output = wxT("--start-output");
const wxString Options::opt_insert_blank = wxT("--insert-blank");
const wxString Options::opt_overwrite = wxT("--overwrite");
const wxString Options::opt_quiet = wxT("--quiet");
const wxString Options::opt_time = wxT("--time");
const wxString Options::opt_version = wxT("--version");

