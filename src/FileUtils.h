#ifndef _FILEUTILS_H_
#define _FILEUTILS_H_

#include <wx/wx.h>

class FileUtils
{
	public:
		static wxString getTmpFileName();
		static wxString getTmpFileName(wxString ext);
		static wxString getQuotedTmpFileName();
		static wxString getQuotedTmpFileName(wxString ext);
		static wxString quoteName(wxString name);
		static void rmFile(wxString path);
};

#endif
