#ifndef _EXECUTER_H_
#define _EXECUTER_H_

#include <wx/wx.h>

class Executer
{
	private:
		wxString args;

		virtual wxString *getCmd() = 0;

	public:
		virtual ~Executer() {}

		long execute(wxTextCtrl *log, bool quiet = false);

		virtual void setArgs(wxString args);
};


#endif
