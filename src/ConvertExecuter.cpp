#include "ConvertExecuter.h"
#include "FileUtils.h"

ConvertExecuter::ConvertExecuter()
{
	cmd = wxT("convert ");
}

wxString *ConvertExecuter::getCmd()
{
	return &(this->cmd);
}

long ConvertExecuter::convert(wxString from, wxString to, wxTextCtrl *log) // names unquoted
{
	wxString *args = new wxString();

	args->Append(FileUtils::quoteName(from));
	args->Append(wxT(" "));
	args->Append(FileUtils::quoteName(to));

	this->setArgs(*args);

	long rv = this->execute(log, false); // TRUE!

	delete args;

	return rv;
}

