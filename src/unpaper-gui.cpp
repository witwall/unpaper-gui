#include "unpaper-gui.h"
#include "MainWindow.h"


IMPLEMENT_APP(wxUnpaperGUI)

wxUnpaperGUI::wxUnpaperGUI()
{
}

wxUnpaperGUI::~wxUnpaperGUI()
{
}

bool wxUnpaperGUI::OnInit()
{
	wxInitAllImageHandlers();

	MainWindow* mainWindow = new MainWindow( (wxWindow*)NULL );
  mainWindow->Show();
  SetTopWindow(mainWindow);
  return true;
}
