///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Apr 17 2008)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "guiBase.h"

///////////////////////////////////////////////////////////////////////////

UnpaperGUIBase::UnpaperGUIBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxSize( 850,600 ), wxDefaultSize );
	
	wxFlexGridSizer* fgSizer1;
	fgSizer1 = new wxFlexGridSizer( 2, 1, 0, 0 );
	fgSizer1->AddGrowableCol( 0 );
	fgSizer1->AddGrowableRow( 0 );
	fgSizer1->SetFlexibleDirection( wxBOTH );
	fgSizer1->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	wxGridSizer* gSizer2;
	gSizer2 = new wxGridSizer( 1, 1, 0, 0 );
	
	m_notebook1 = new wxNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );
	m_panel1 = new wxPanel( m_notebook1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer1;
	bSizer1 = new wxBoxSizer( wxVERTICAL );
	
	m_splitter1 = new wxSplitterWindow( m_panel1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D );
	m_splitter1->SetMinimumPaneSize( 1 );
	m_splitter1->Connect( wxEVT_IDLE, wxIdleEventHandler( UnpaperGUIBase::m_splitter1OnIdle ), NULL, this );
	m_panel6 = new wxPanel( m_splitter1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSUNKEN_BORDER|wxTAB_TRAVERSAL );
	wxFlexGridSizer* fgSizer2;
	fgSizer2 = new wxFlexGridSizer( 2, 1, 0, 0 );
	fgSizer2->AddGrowableCol( 0 );
	fgSizer2->AddGrowableRow( 1 );
	fgSizer2->SetFlexibleDirection( wxBOTH );
	fgSizer2->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_staticText1 = new wxStaticText( m_panel6, wxID_ANY, wxT("Before"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1->Wrap( -1 );
	fgSizer2->Add( m_staticText1, 0, wxALIGN_CENTER|wxALL, 5 );
	
	beforeScrolledWindow = new wxScrolledWindow( m_panel6, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHSCROLL|wxVSCROLL );
	beforeScrolledWindow->SetScrollRate( 5, 5 );
	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer( wxVERTICAL );
	
	beforeBitmap = new wxStaticBitmap( beforeScrolledWindow, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer3->Add( beforeBitmap, 1, wxALL|wxEXPAND, 5 );
	
	beforeScrolledWindow->SetSizer( bSizer3 );
	beforeScrolledWindow->Layout();
	bSizer3->Fit( beforeScrolledWindow );
	fgSizer2->Add( beforeScrolledWindow, 1, wxEXPAND | wxALL, 5 );
	
	m_panel6->SetSizer( fgSizer2 );
	m_panel6->Layout();
	fgSizer2->Fit( m_panel6 );
	m_panel23 = new wxPanel( m_splitter1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer29;
	bSizer29 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText2 = new wxStaticText( m_panel23, wxID_ANY, wxT("After"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2->Wrap( -1 );
	bSizer29->Add( m_staticText2, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_splitter3 = new wxSplitterWindow( m_panel23, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D );
	m_splitter3->SetMinimumPaneSize( 1 );
	m_splitter3->Connect( wxEVT_IDLE, wxIdleEventHandler( UnpaperGUIBase::m_splitter3OnIdle ), NULL, this );
	m_panel7 = new wxPanel( m_splitter3, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSUNKEN_BORDER|wxTAB_TRAVERSAL );
	wxFlexGridSizer* fgSizer21;
	fgSizer21 = new wxFlexGridSizer( 2, 1, 0, 0 );
	fgSizer21->AddGrowableCol( 0 );
	fgSizer21->AddGrowableRow( 0 );
	fgSizer21->SetFlexibleDirection( wxBOTH );
	fgSizer21->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	after1ScrolledWindow = new wxScrolledWindow( m_panel7, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHSCROLL|wxVSCROLL );
	after1ScrolledWindow->SetScrollRate( 5, 5 );
	wxBoxSizer* bSizer31;
	bSizer31 = new wxBoxSizer( wxVERTICAL );
	
	after1Bitmap = new wxStaticBitmap( after1ScrolledWindow, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer31->Add( after1Bitmap, 0, wxALL|wxEXPAND, 5 );
	
	after1ScrolledWindow->SetSizer( bSizer31 );
	after1ScrolledWindow->Layout();
	bSizer31->Fit( after1ScrolledWindow );
	fgSizer21->Add( after1ScrolledWindow, 1, wxEXPAND | wxALL, 5 );
	
	wxBoxSizer* bSizer101;
	bSizer101 = new wxBoxSizer( wxHORIZONTAL );
	
	live1Preview = new wxCheckBox( m_panel7, wxID_ANY, wxT("Live preview"), wxDefaultPosition, wxDefaultSize, 0 );
	
	bSizer101->Add( live1Preview, 0, wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxALL, 5 );
	
	m_button2 = new wxButton( m_panel7, wxID_ANY, wxT("Preview"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer101->Add( m_button2, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	fgSizer21->Add( bSizer101, 0, wxALIGN_RIGHT|wxEXPAND, 5 );
	
	m_panel7->SetSizer( fgSizer21 );
	m_panel7->Layout();
	fgSizer21->Fit( m_panel7 );
	m_panel25 = new wxPanel( m_splitter3, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSUNKEN_BORDER|wxTAB_TRAVERSAL );
	wxFlexGridSizer* fgSizer211;
	fgSizer211 = new wxFlexGridSizer( 2, 1, 0, 0 );
	fgSizer211->AddGrowableCol( 0 );
	fgSizer211->AddGrowableRow( 0 );
	fgSizer211->SetFlexibleDirection( wxBOTH );
	fgSizer211->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	after2ScrolledWindow = new wxScrolledWindow( m_panel25, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHSCROLL|wxVSCROLL );
	after2ScrolledWindow->SetScrollRate( 5, 5 );
	wxBoxSizer* bSizer311;
	bSizer311 = new wxBoxSizer( wxVERTICAL );
	
	after2Bitmap = new wxStaticBitmap( after2ScrolledWindow, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer311->Add( after2Bitmap, 0, wxALL|wxEXPAND, 5 );
	
	after2ScrolledWindow->SetSizer( bSizer311 );
	after2ScrolledWindow->Layout();
	bSizer311->Fit( after2ScrolledWindow );
	fgSizer211->Add( after2ScrolledWindow, 1, wxEXPAND | wxALL, 5 );
	
	wxBoxSizer* bSizer1011;
	bSizer1011 = new wxBoxSizer( wxHORIZONTAL );
	
	live2Preview = new wxCheckBox( m_panel25, wxID_ANY, wxT("Live preview"), wxDefaultPosition, wxDefaultSize, 0 );
	
	bSizer1011->Add( live2Preview, 0, wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxALL, 5 );
	
	m_button21 = new wxButton( m_panel25, wxID_ANY, wxT("Preview"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer1011->Add( m_button21, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	fgSizer211->Add( bSizer1011, 0, wxALIGN_RIGHT|wxEXPAND, 5 );
	
	m_panel25->SetSizer( fgSizer211 );
	m_panel25->Layout();
	fgSizer211->Fit( m_panel25 );
	m_splitter3->SplitVertically( m_panel7, m_panel25, 216 );
	bSizer29->Add( m_splitter3, 1, wxEXPAND, 5 );
	
	m_panel23->SetSizer( bSizer29 );
	m_panel23->Layout();
	bSizer29->Fit( m_panel23 );
	m_splitter1->SplitVertically( m_panel6, m_panel23, 428 );
	bSizer1->Add( m_splitter1, 1, wxEXPAND, 5 );
	
	wxGridSizer* gSizer10;
	gSizer10 = new wxGridSizer( 2, 2, 0, 0 );
	
	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText3 = new wxStaticText( m_panel1, wxID_ANY, wxT("Zoom"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText3->Wrap( -1 );
	bSizer2->Add( m_staticText3, 0, wxALIGN_CENTER|wxALL, 5 );
	
	scaleCombo = new wxComboBox( m_panel1, wxID_ANY, wxT("100"), wxDefaultPosition, wxDefaultSize, 0, NULL, wxCB_DROPDOWN|wxCB_READONLY|wxCB_SORT|wxTE_PROCESS_ENTER );
	scaleCombo->Append( wxT("10") );
	scaleCombo->Append( wxT("25") );
	scaleCombo->Append( wxT("50") );
	scaleCombo->Append( wxT("100") );
	scaleCombo->Append( wxT("200") );
	scaleCombo->Append( wxT("400") );
	bSizer2->Add( scaleCombo, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_staticText4 = new wxStaticText( m_panel1, wxID_ANY, wxT("%"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText4->Wrap( -1 );
	bSizer2->Add( m_staticText4, 0, wxALIGN_CENTER|wxALL, 5 );
	
	gSizer10->Add( bSizer2, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer86;
	bSizer86 = new wxBoxSizer( wxHORIZONTAL );
	
	synchroScrolls = new wxCheckBox( m_panel1, wxID_ANY, wxT("Synchronize scrollbars"), wxDefaultPosition, wxDefaultSize, 0 );
	synchroScrolls->SetValue(true);
	
	bSizer86->Add( synchroScrolls, 0, wxALIGN_CENTER|wxALL, 5 );
	
	gSizer10->Add( bSizer86, 1, wxEXPAND, 5 );
	
	bSizer1->Add( gSizer10, 0, wxEXPAND, 5 );
	
	m_panel1->SetSizer( bSizer1 );
	m_panel1->Layout();
	bSizer1->Fit( m_panel1 );
	m_notebook1->AddPage( m_panel1, wxT("Preview"), true );
	m_panel3 = new wxPanel( m_notebook1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxFlexGridSizer* fgSizer5;
	fgSizer5 = new wxFlexGridSizer( 2, 1, 0, 0 );
	fgSizer5->AddGrowableCol( 0 );
	fgSizer5->AddGrowableRow( 0 );
	fgSizer5->SetFlexibleDirection( wxBOTH );
	fgSizer5->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	logTextCtrl = new wxTextCtrl( m_panel3, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE|wxTE_READONLY );
	fgSizer5->Add( logTextCtrl, 0, wxALL|wxEXPAND, 5 );
	
	m_button3 = new wxButton( m_panel3, wxID_ANY, wxT("Clear"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer5->Add( m_button3, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_panel3->SetSizer( fgSizer5 );
	m_panel3->Layout();
	fgSizer5->Fit( m_panel3 );
	m_notebook1->AddPage( m_panel3, wxT("Log"), false );
	
	gSizer2->Add( m_notebook1, 1, wxALL|wxEXPAND, 5 );
	
	fgSizer1->Add( gSizer2, 1, wxEXPAND, 5 );
	
	wxGridSizer* gSizer3;
	gSizer3 = new wxGridSizer( 1, 1, 0, 0 );
	
	m_notebook2 = new wxNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );
	m_panel4 = new wxPanel( m_notebook2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxGridSizer* gSizer4;
	gSizer4 = new wxGridSizer( 2, 2, 0, 0 );
	
	wxBoxSizer* bSizer112124;
	bSizer112124 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText61124 = new wxStaticText( m_panel4, wxID_ANY, wxT("white treshold"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText61124->Wrap( -1 );
	bSizer112124->Add( m_staticText61124, 0, wxALIGN_CENTER|wxALL, 5 );
	
	whiteTreshold = new wxTextCtrl( m_panel4, wxID_ANY, wxT("0.9"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer112124->Add( whiteTreshold, 0, wxALIGN_CENTER|wxALL, 5 );
	
	gSizer4->Add( bSizer112124, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer1121241;
	bSizer1121241 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText611241 = new wxStaticText( m_panel4, wxID_ANY, wxT("black treshold"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText611241->Wrap( -1 );
	bSizer1121241->Add( m_staticText611241, 0, wxALIGN_CENTER|wxALL, 5 );
	
	blackTreshold = new wxTextCtrl( m_panel4, wxID_ANY, wxT("0.33"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer1121241->Add( blackTreshold, 0, wxALIGN_CENTER|wxALL, 5 );
	
	gSizer4->Add( bSizer1121241, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer1121242;
	bSizer1121242 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText611242 = new wxStaticText( m_panel4, wxID_ANY, wxT("layout"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText611242->Wrap( -1 );
	bSizer1121242->Add( m_staticText611242, 0, wxALIGN_CENTER|wxALL, 5 );
	
	layoutSingle = new wxRadioButton( m_panel4, wxID_ANY, wxT("single"), wxDefaultPosition, wxDefaultSize, 0 );
	layoutSingle->SetValue( true ); 
	bSizer1121242->Add( layoutSingle, 0, wxALIGN_CENTER|wxALL, 5 );
	
	layoutDouble = new wxRadioButton( m_panel4, wxID_ANY, wxT("double"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer1121242->Add( layoutDouble, 0, wxALIGN_CENTER|wxALL, 5 );
	
	layoutNone = new wxRadioButton( m_panel4, wxID_ANY, wxT("none"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer1121242->Add( layoutNone, 0, wxALIGN_CENTER|wxALL, 5 );
	
	gSizer4->Add( bSizer1121242, 1, wxEXPAND, 5 );
	
	m_panel4->SetSizer( gSizer4 );
	m_panel4->Layout();
	gSizer4->Fit( m_panel4 );
	m_notebook2->AddPage( m_panel4, wxT("General"), true );
	m_panel5 = new wxPanel( m_notebook2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxGridSizer* gSizer51;
	gSizer51 = new wxGridSizer( 2, 2, 0, 0 );
	
	wxBoxSizer* bSizer1122;
	bSizer1122 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText612 = new wxStaticText( m_panel5, wxID_ANY, wxT("rotate"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText612->Wrap( -1 );
	bSizer1122->Add( m_staticText612, 0, wxALIGN_CENTER|wxALL, 5 );
	
	wxString preRotateChoices[] = { wxEmptyString, wxT("90"), wxT("-90") };
	int preRotateNChoices = sizeof( preRotateChoices ) / sizeof( wxString );
	preRotate = new wxChoice( m_panel5, wxID_ANY, wxDefaultPosition, wxDefaultSize, preRotateNChoices, preRotateChoices, 0 );
	preRotate->SetSelection( 0 );
	bSizer1122->Add( preRotate, 0, wxALIGN_CENTER|wxALL, 5 );
	
	gSizer51->Add( bSizer1122, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer211;
	bSizer211 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText81 = new wxStaticText( m_panel5, wxID_ANY, wxT("mirror"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText81->Wrap( -1 );
	bSizer211->Add( m_staticText81, 0, wxALIGN_CENTER|wxALL, 5 );
	
	preMirrorHorizontal = new wxCheckBox( m_panel5, wxID_ANY, wxT("horizontal"), wxDefaultPosition, wxDefaultSize, 0 );
	
	bSizer211->Add( preMirrorHorizontal, 0, wxALIGN_CENTER|wxALL, 5 );
	
	preMirrorVertical = new wxCheckBox( m_panel5, wxID_ANY, wxT("vertical"), wxDefaultPosition, wxDefaultSize, 0 );
	
	bSizer211->Add( preMirrorVertical, 0, wxALIGN_CENTER|wxALL, 5 );
	
	gSizer51->Add( bSizer211, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer2421;
	bSizer2421 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText921 = new wxStaticText( m_panel5, wxID_ANY, wxT("shift"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText921->Wrap( -1 );
	bSizer2421->Add( m_staticText921, 0, wxALIGN_CENTER|wxALL, 5 );
	
	wxBoxSizer* bSizer2521;
	bSizer2521 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer2631;
	bSizer2631 = new wxBoxSizer( wxHORIZONTAL );
	
	preShiftHorizontal = new wxTextCtrl( m_panel5, wxID_ANY, wxT("0"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer2631->Add( preShiftHorizontal, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_staticText1031 = new wxStaticText( m_panel5, wxID_ANY, wxT("horizontal"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1031->Wrap( -1 );
	bSizer2631->Add( m_staticText1031, 0, wxALIGN_CENTER|wxALL, 5 );
	
	bSizer2521->Add( bSizer2631, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer26121;
	bSizer26121 = new wxBoxSizer( wxHORIZONTAL );
	
	preShiftVertical = new wxTextCtrl( m_panel5, wxID_ANY, wxT("0"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer26121->Add( preShiftVertical, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_staticText10121 = new wxStaticText( m_panel5, wxID_ANY, wxT("vertical"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText10121->Wrap( -1 );
	bSizer26121->Add( m_staticText10121, 0, wxALIGN_CENTER|wxALL, 5 );
	
	bSizer2521->Add( bSizer26121, 1, wxEXPAND, 5 );
	
	bSizer2421->Add( bSizer2521, 1, wxEXPAND, 5 );
	
	gSizer51->Add( bSizer2421, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer24211;
	bSizer24211 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText9211 = new wxStaticText( m_panel5, wxID_ANY, wxT("size"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText9211->Wrap( -1 );
	bSizer24211->Add( m_staticText9211, 0, wxALIGN_CENTER|wxALL, 5 );
	
	wxBoxSizer* bSizer25211;
	bSizer25211 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer26311;
	bSizer26311 = new wxBoxSizer( wxHORIZONTAL );
	
	wxString sizeChoiceChoices[] = { wxEmptyString, wxT("legal"), wxT("letter"), wxT("a3"), wxT("a4"), wxT("a5") };
	int sizeChoiceNChoices = sizeof( sizeChoiceChoices ) / sizeof( wxString );
	sizeChoice = new wxChoice( m_panel5, wxID_ANY, wxDefaultPosition, wxDefaultSize, sizeChoiceNChoices, sizeChoiceChoices, 0 );
	sizeChoice->SetSelection( 0 );
	bSizer26311->Add( sizeChoice, 0, wxALIGN_CENTER|wxALL, 5 );
	
	bSizer25211->Add( bSizer26311, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer261211;
	bSizer261211 = new wxBoxSizer( wxHORIZONTAL );
	
	landscapeCheck = new wxCheckBox( m_panel5, wxID_ANY, wxT("landscape"), wxDefaultPosition, wxDefaultSize, 0 );
	
	bSizer261211->Add( landscapeCheck, 0, wxALIGN_CENTER|wxALL, 5 );
	
	bSizer25211->Add( bSizer261211, 1, wxEXPAND, 5 );
	
	bSizer24211->Add( bSizer25211, 1, wxEXPAND, 5 );
	
	gSizer51->Add( bSizer24211, 1, wxEXPAND, 5 );
	
	m_panel5->SetSizer( gSizer51 );
	m_panel5->Layout();
	gSizer51->Fit( m_panel5 );
	m_notebook2->AddPage( m_panel5, wxT("Preprocess"), false );
	m_panel9 = new wxPanel( m_notebook2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxGridSizer* gSizer511;
	gSizer511 = new wxGridSizer( 2, 2, 0, 0 );
	
	wxBoxSizer* bSizer11221;
	bSizer11221 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText6121 = new wxStaticText( m_panel9, wxID_ANY, wxT("rotate"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText6121->Wrap( -1 );
	bSizer11221->Add( m_staticText6121, 0, wxALIGN_CENTER|wxALL, 5 );
	
	wxString postRotateChoices[] = { wxEmptyString, wxT("90"), wxT("-90") };
	int postRotateNChoices = sizeof( postRotateChoices ) / sizeof( wxString );
	postRotate = new wxChoice( m_panel9, wxID_ANY, wxDefaultPosition, wxDefaultSize, postRotateNChoices, postRotateChoices, 0 );
	postRotate->SetSelection( 0 );
	bSizer11221->Add( postRotate, 0, wxALIGN_CENTER|wxALL, 5 );
	
	gSizer511->Add( bSizer11221, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer2111;
	bSizer2111 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText811 = new wxStaticText( m_panel9, wxID_ANY, wxT("mirror"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText811->Wrap( -1 );
	bSizer2111->Add( m_staticText811, 0, wxALIGN_CENTER|wxALL, 5 );
	
	postMirrorHorizontal = new wxCheckBox( m_panel9, wxID_ANY, wxT("horizontal"), wxDefaultPosition, wxDefaultSize, 0 );
	
	bSizer2111->Add( postMirrorHorizontal, 0, wxALIGN_CENTER|wxALL, 5 );
	
	postMirrorVertical = new wxCheckBox( m_panel9, wxID_ANY, wxT("vertical"), wxDefaultPosition, wxDefaultSize, 0 );
	
	bSizer2111->Add( postMirrorVertical, 0, wxALIGN_CENTER|wxALL, 5 );
	
	gSizer511->Add( bSizer2111, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer24212;
	bSizer24212 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText9212 = new wxStaticText( m_panel9, wxID_ANY, wxT("shift"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText9212->Wrap( -1 );
	bSizer24212->Add( m_staticText9212, 0, wxALIGN_CENTER|wxALL, 5 );
	
	wxBoxSizer* bSizer25212;
	bSizer25212 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer26312;
	bSizer26312 = new wxBoxSizer( wxHORIZONTAL );
	
	postShiftHorizontal = new wxTextCtrl( m_panel9, wxID_ANY, wxT("0"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer26312->Add( postShiftHorizontal, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_staticText10311 = new wxStaticText( m_panel9, wxID_ANY, wxT("horizontal"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText10311->Wrap( -1 );
	bSizer26312->Add( m_staticText10311, 0, wxALIGN_CENTER|wxALL, 5 );
	
	bSizer25212->Add( bSizer26312, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer261212;
	bSizer261212 = new wxBoxSizer( wxHORIZONTAL );
	
	postShiftVertical = new wxTextCtrl( m_panel9, wxID_ANY, wxT("0"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer261212->Add( postShiftVertical, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_staticText101211 = new wxStaticText( m_panel9, wxID_ANY, wxT("vertical"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText101211->Wrap( -1 );
	bSizer261212->Add( m_staticText101211, 0, wxALIGN_CENTER|wxALL, 5 );
	
	bSizer25212->Add( bSizer261212, 1, wxEXPAND, 5 );
	
	bSizer24212->Add( bSizer25212, 1, wxEXPAND, 5 );
	
	gSizer511->Add( bSizer24212, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer242111;
	bSizer242111 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText92111 = new wxStaticText( m_panel9, wxID_ANY, wxT("size"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText92111->Wrap( -1 );
	bSizer242111->Add( m_staticText92111, 0, wxALIGN_CENTER|wxALL, 5 );
	
	wxBoxSizer* bSizer252111;
	bSizer252111 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer263111;
	bSizer263111 = new wxBoxSizer( wxHORIZONTAL );
	
	wxString postSizeChoices[] = { wxEmptyString, wxT("legal"), wxT("letter"), wxT("a3"), wxT("a4"), wxT("a5") };
	int postSizeNChoices = sizeof( postSizeChoices ) / sizeof( wxString );
	postSize = new wxChoice( m_panel9, wxID_ANY, wxDefaultPosition, wxDefaultSize, postSizeNChoices, postSizeChoices, 0 );
	postSize->SetSelection( 0 );
	bSizer263111->Add( postSize, 0, wxALIGN_CENTER|wxALL, 5 );
	
	bSizer252111->Add( bSizer263111, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer2612111;
	bSizer2612111 = new wxBoxSizer( wxHORIZONTAL );
	
	postLandscapeCheck = new wxCheckBox( m_panel9, wxID_ANY, wxT("landscape"), wxDefaultPosition, wxDefaultSize, 0 );
	
	bSizer2612111->Add( postLandscapeCheck, 0, wxALIGN_CENTER|wxALL, 5 );
	
	bSizer252111->Add( bSizer2612111, 1, wxEXPAND, 5 );
	
	bSizer242111->Add( bSizer252111, 1, wxEXPAND, 5 );
	
	gSizer511->Add( bSizer242111, 1, wxEXPAND, 5 );
	
	m_panel9->SetSizer( gSizer511 );
	m_panel9->Layout();
	gSizer511->Fit( m_panel9 );
	m_notebook2->AddPage( m_panel9, wxT("Postprocess"), false );
	m_panel11 = new wxPanel( m_notebook2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer61;
	bSizer61 = new wxBoxSizer( wxHORIZONTAL );
	
	m_panel201 = new wxPanel( m_panel11, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer7;
	bSizer7 = new wxBoxSizer( wxVERTICAL );
	
	noiseDisable = new wxRadioButton( m_panel201, wxID_ANY, wxT("disable"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer7->Add( noiseDisable, 0, wxALL, 5 );
	
	noiseManual = new wxRadioButton( m_panel201, wxID_ANY, wxT("manual"), wxDefaultPosition, wxDefaultSize, 0 );
	noiseManual->SetValue( true ); 
	bSizer7->Add( noiseManual, 0, wxALL, 5 );
	
	m_panel201->SetSizer( bSizer7 );
	m_panel201->Layout();
	bSizer7->Fit( m_panel201 );
	bSizer61->Add( m_panel201, 0, wxEXPAND | wxALL, 5 );
	
	m_panel21 = new wxPanel( m_panel11, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer10;
	bSizer10 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer11;
	bSizer11 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText6 = new wxStaticText( m_panel21, wxID_ANY, wxT("intensity"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText6->Wrap( -1 );
	bSizer11->Add( m_staticText6, 0, wxALIGN_CENTER|wxALL, 5 );
	
	noiseIntensity = new wxTextCtrl( m_panel21, wxID_ANY, wxT("4"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer11->Add( noiseIntensity, 0, wxALIGN_CENTER|wxALL, 5 );
	
	bSizer10->Add( bSizer11, 1, wxEXPAND, 5 );
	
	m_panel21->SetSizer( bSizer10 );
	m_panel21->Layout();
	bSizer10->Fit( m_panel21 );
	bSizer61->Add( m_panel21, 1, wxEXPAND | wxALL, 5 );
	
	m_panel11->SetSizer( bSizer61 );
	m_panel11->Layout();
	bSizer61->Fit( m_panel11 );
	m_notebook2->AddPage( m_panel11, wxT("Noise filter"), false );
	m_panel12 = new wxPanel( m_notebook2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer611;
	bSizer611 = new wxBoxSizer( wxHORIZONTAL );
	
	m_panel2011 = new wxPanel( m_panel12, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer71;
	bSizer71 = new wxBoxSizer( wxVERTICAL );
	
	blackDisable = new wxRadioButton( m_panel2011, wxID_ANY, wxT("disable"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer71->Add( blackDisable, 0, wxALL, 5 );
	
	blackManual = new wxRadioButton( m_panel2011, wxID_ANY, wxT("manual"), wxDefaultPosition, wxDefaultSize, 0 );
	blackManual->SetValue( true ); 
	bSizer71->Add( blackManual, 0, wxALL, 5 );
	
	m_panel2011->SetSizer( bSizer71 );
	m_panel2011->Layout();
	bSizer71->Fit( m_panel2011 );
	bSizer611->Add( m_panel2011, 0, wxEXPAND | wxALL, 5 );
	
	m_panel211 = new wxPanel( m_panel12, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxGridSizer* gSizer5;
	gSizer5 = new wxGridSizer( 2, 3, 0, 0 );
	
	wxBoxSizer* bSizer112;
	bSizer112 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText61 = new wxStaticText( m_panel211, wxID_ANY, wxT("intensity"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText61->Wrap( -1 );
	bSizer112->Add( m_staticText61, 0, wxALIGN_CENTER|wxALL, 5 );
	
	blackIntensity = new wxTextCtrl( m_panel211, wxID_ANY, wxT("4"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer112->Add( blackIntensity, 0, wxALIGN_CENTER|wxALL, 5 );
	
	gSizer5->Add( bSizer112, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer21;
	bSizer21 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText8 = new wxStaticText( m_panel211, wxID_ANY, wxT("scan direction"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText8->Wrap( -1 );
	bSizer21->Add( m_staticText8, 0, wxALIGN_CENTER|wxALL, 5 );
	
	blackScanHorizontal = new wxCheckBox( m_panel211, wxID_ANY, wxT("horizontal"), wxDefaultPosition, wxDefaultSize, 0 );
	blackScanHorizontal->SetValue(true);
	
	bSizer21->Add( blackScanHorizontal, 0, wxALIGN_CENTER|wxALL, 5 );
	
	blackScanVertical = new wxCheckBox( m_panel211, wxID_ANY, wxT("vertical"), wxDefaultPosition, wxDefaultSize, 0 );
	blackScanVertical->SetValue(true);
	
	bSizer21->Add( blackScanVertical, 0, wxALIGN_CENTER|wxALL, 5 );
	
	gSizer5->Add( bSizer21, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer24;
	bSizer24 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText9 = new wxStaticText( m_panel211, wxID_ANY, wxT("scan size"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText9->Wrap( -1 );
	bSizer24->Add( m_staticText9, 0, wxALIGN_CENTER|wxALL, 5 );
	
	wxBoxSizer* bSizer25;
	bSizer25 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer26;
	bSizer26 = new wxBoxSizer( wxHORIZONTAL );
	
	blackScanSizeHorizontal = new wxTextCtrl( m_panel211, wxID_ANY, wxT("20"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer26->Add( blackScanSizeHorizontal, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_staticText10 = new wxStaticText( m_panel211, wxID_ANY, wxT("horizontal"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText10->Wrap( -1 );
	bSizer26->Add( m_staticText10, 0, wxALIGN_CENTER|wxALL, 5 );
	
	bSizer25->Add( bSizer26, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer261;
	bSizer261 = new wxBoxSizer( wxHORIZONTAL );
	
	blackScanSizeVertical = new wxTextCtrl( m_panel211, wxID_ANY, wxT("20"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer261->Add( blackScanSizeVertical, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_staticText101 = new wxStaticText( m_panel211, wxID_ANY, wxT("vertical"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText101->Wrap( -1 );
	bSizer261->Add( m_staticText101, 0, wxALIGN_CENTER|wxALL, 5 );
	
	bSizer25->Add( bSizer261, 1, wxEXPAND, 5 );
	
	bSizer24->Add( bSizer25, 1, wxEXPAND, 5 );
	
	gSizer5->Add( bSizer24, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer241;
	bSizer241 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText91 = new wxStaticText( m_panel211, wxID_ANY, wxT("scan depth"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText91->Wrap( -1 );
	bSizer241->Add( m_staticText91, 0, wxALIGN_CENTER|wxALL, 5 );
	
	wxBoxSizer* bSizer251;
	bSizer251 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer262;
	bSizer262 = new wxBoxSizer( wxHORIZONTAL );
	
	blackScanDepthHorizontal = new wxTextCtrl( m_panel211, wxID_ANY, wxT("500"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer262->Add( blackScanDepthHorizontal, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_staticText102 = new wxStaticText( m_panel211, wxID_ANY, wxT("horizontal"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText102->Wrap( -1 );
	bSizer262->Add( m_staticText102, 0, wxALIGN_CENTER|wxALL, 5 );
	
	bSizer251->Add( bSizer262, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer2611;
	bSizer2611 = new wxBoxSizer( wxHORIZONTAL );
	
	blackScanDepthVertical = new wxTextCtrl( m_panel211, wxID_ANY, wxT("500"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer2611->Add( blackScanDepthVertical, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_staticText1011 = new wxStaticText( m_panel211, wxID_ANY, wxT("vertical"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1011->Wrap( -1 );
	bSizer2611->Add( m_staticText1011, 0, wxALIGN_CENTER|wxALL, 5 );
	
	bSizer251->Add( bSizer2611, 1, wxEXPAND, 5 );
	
	bSizer241->Add( bSizer251, 1, wxEXPAND, 5 );
	
	gSizer5->Add( bSizer241, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer242;
	bSizer242 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText92 = new wxStaticText( m_panel211, wxID_ANY, wxT("scan step"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText92->Wrap( -1 );
	bSizer242->Add( m_staticText92, 0, wxALIGN_CENTER|wxALL, 5 );
	
	wxBoxSizer* bSizer252;
	bSizer252 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer263;
	bSizer263 = new wxBoxSizer( wxHORIZONTAL );
	
	blackScanStepHorizontal = new wxTextCtrl( m_panel211, wxID_ANY, wxT("5"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer263->Add( blackScanStepHorizontal, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_staticText103 = new wxStaticText( m_panel211, wxID_ANY, wxT("horizontal"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText103->Wrap( -1 );
	bSizer263->Add( m_staticText103, 0, wxALIGN_CENTER|wxALL, 5 );
	
	bSizer252->Add( bSizer263, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer2612;
	bSizer2612 = new wxBoxSizer( wxHORIZONTAL );
	
	blackScanStepVertical = new wxTextCtrl( m_panel211, wxID_ANY, wxT("5"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer2612->Add( blackScanStepVertical, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_staticText1012 = new wxStaticText( m_panel211, wxID_ANY, wxT("vertical"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1012->Wrap( -1 );
	bSizer2612->Add( m_staticText1012, 0, wxALIGN_CENTER|wxALL, 5 );
	
	bSizer252->Add( bSizer2612, 1, wxEXPAND, 5 );
	
	bSizer242->Add( bSizer252, 1, wxEXPAND, 5 );
	
	gSizer5->Add( bSizer242, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer1121;
	bSizer1121 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText611 = new wxStaticText( m_panel211, wxID_ANY, wxT("scan threshold"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText611->Wrap( -1 );
	bSizer1121->Add( m_staticText611, 0, wxALIGN_CENTER|wxALL, 5 );
	
	blackScanThreshold = new wxTextCtrl( m_panel211, wxID_ANY, wxT("0.95"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer1121->Add( blackScanThreshold, 0, wxALIGN_CENTER|wxALL, 5 );
	
	gSizer5->Add( bSizer1121, 1, wxEXPAND, 5 );
	
	m_panel211->SetSizer( gSizer5 );
	m_panel211->Layout();
	gSizer5->Fit( m_panel211 );
	bSizer611->Add( m_panel211, 1, wxEXPAND | wxALL, 5 );
	
	m_panel12->SetSizer( bSizer611 );
	m_panel12->Layout();
	bSizer611->Fit( m_panel12 );
	m_notebook2->AddPage( m_panel12, wxT("Black filter"), false );
	m_panel13 = new wxPanel( m_notebook2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer6111;
	bSizer6111 = new wxBoxSizer( wxHORIZONTAL );
	
	m_panel20111 = new wxPanel( m_panel13, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer711;
	bSizer711 = new wxBoxSizer( wxVERTICAL );
	
	blurDisable = new wxRadioButton( m_panel20111, wxID_ANY, wxT("disable"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer711->Add( blurDisable, 0, wxALL, 5 );
	
	blurManual = new wxRadioButton( m_panel20111, wxID_ANY, wxT("manual"), wxDefaultPosition, wxDefaultSize, 0 );
	blurManual->SetValue( true ); 
	bSizer711->Add( blurManual, 0, wxALL, 5 );
	
	m_panel20111->SetSizer( bSizer711 );
	m_panel20111->Layout();
	bSizer711->Fit( m_panel20111 );
	bSizer6111->Add( m_panel20111, 0, wxEXPAND | wxALL, 5 );
	
	m_panel2111 = new wxPanel( m_panel13, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxGridSizer* gSizer52;
	gSizer52 = new wxGridSizer( 2, 3, 0, 0 );
	
	wxBoxSizer* bSizer1123;
	bSizer1123 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText613 = new wxStaticText( m_panel2111, wxID_ANY, wxT("intensity"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText613->Wrap( -1 );
	bSizer1123->Add( m_staticText613, 0, wxALIGN_CENTER|wxALL, 5 );
	
	blurIntensity = new wxTextCtrl( m_panel2111, wxID_ANY, wxT("0.01"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer1123->Add( blurIntensity, 0, wxALIGN_CENTER|wxALL, 5 );
	
	gSizer52->Add( bSizer1123, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer243;
	bSizer243 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText93 = new wxStaticText( m_panel2111, wxID_ANY, wxT("size"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText93->Wrap( -1 );
	bSizer243->Add( m_staticText93, 0, wxALIGN_CENTER|wxALL, 5 );
	
	wxBoxSizer* bSizer253;
	bSizer253 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer264;
	bSizer264 = new wxBoxSizer( wxHORIZONTAL );
	
	blurSizeHorizontal = new wxTextCtrl( m_panel2111, wxID_ANY, wxT("100"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer264->Add( blurSizeHorizontal, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_staticText104 = new wxStaticText( m_panel2111, wxID_ANY, wxT("horizontal"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText104->Wrap( -1 );
	bSizer264->Add( m_staticText104, 0, wxALIGN_CENTER|wxALL, 5 );
	
	bSizer253->Add( bSizer264, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer2613;
	bSizer2613 = new wxBoxSizer( wxHORIZONTAL );
	
	blurSizeVertical = new wxTextCtrl( m_panel2111, wxID_ANY, wxT("100"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer2613->Add( blurSizeVertical, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_staticText1013 = new wxStaticText( m_panel2111, wxID_ANY, wxT("vertical"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1013->Wrap( -1 );
	bSizer2613->Add( m_staticText1013, 0, wxALIGN_CENTER|wxALL, 5 );
	
	bSizer253->Add( bSizer2613, 1, wxEXPAND, 5 );
	
	bSizer243->Add( bSizer253, 1, wxEXPAND, 5 );
	
	gSizer52->Add( bSizer243, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer2422;
	bSizer2422 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText922 = new wxStaticText( m_panel2111, wxID_ANY, wxT("step"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText922->Wrap( -1 );
	bSizer2422->Add( m_staticText922, 0, wxALIGN_CENTER|wxALL, 5 );
	
	wxBoxSizer* bSizer2522;
	bSizer2522 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer2632;
	bSizer2632 = new wxBoxSizer( wxHORIZONTAL );
	
	blurStepHorizontal = new wxTextCtrl( m_panel2111, wxID_ANY, wxT("50"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer2632->Add( blurStepHorizontal, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_staticText1032 = new wxStaticText( m_panel2111, wxID_ANY, wxT("horizontal"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1032->Wrap( -1 );
	bSizer2632->Add( m_staticText1032, 0, wxALIGN_CENTER|wxALL, 5 );
	
	bSizer2522->Add( bSizer2632, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer26122;
	bSizer26122 = new wxBoxSizer( wxHORIZONTAL );
	
	blurStepVertical = new wxTextCtrl( m_panel2111, wxID_ANY, wxT("50"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer26122->Add( blurStepVertical, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_staticText10122 = new wxStaticText( m_panel2111, wxID_ANY, wxT("vertical"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText10122->Wrap( -1 );
	bSizer26122->Add( m_staticText10122, 0, wxALIGN_CENTER|wxALL, 5 );
	
	bSizer2522->Add( bSizer26122, 1, wxEXPAND, 5 );
	
	bSizer2422->Add( bSizer2522, 1, wxEXPAND, 5 );
	
	gSizer52->Add( bSizer2422, 1, wxEXPAND, 5 );
	
	m_panel2111->SetSizer( gSizer52 );
	m_panel2111->Layout();
	gSizer52->Fit( m_panel2111 );
	bSizer6111->Add( m_panel2111, 1, wxEXPAND | wxALL, 5 );
	
	m_panel13->SetSizer( bSizer6111 );
	m_panel13->Layout();
	bSizer6111->Fit( m_panel13 );
	m_notebook2->AddPage( m_panel13, wxT("Blur filter"), false );
	m_panel14 = new wxPanel( m_notebook2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer6112;
	bSizer6112 = new wxBoxSizer( wxHORIZONTAL );
	
	m_panel20112 = new wxPanel( m_panel14, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer712;
	bSizer712 = new wxBoxSizer( wxVERTICAL );
	
	grayDisable = new wxRadioButton( m_panel20112, wxID_ANY, wxT("disable"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer712->Add( grayDisable, 0, wxALL, 5 );
	
	grayManual = new wxRadioButton( m_panel20112, wxID_ANY, wxT("manual"), wxDefaultPosition, wxDefaultSize, 0 );
	grayManual->SetValue( true ); 
	bSizer712->Add( grayManual, 0, wxALL, 5 );
	
	m_panel20112->SetSizer( bSizer712 );
	m_panel20112->Layout();
	bSizer712->Fit( m_panel20112 );
	bSizer6112->Add( m_panel20112, 0, wxEXPAND | wxALL, 5 );
	
	m_panel2112 = new wxPanel( m_panel14, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxGridSizer* gSizer53;
	gSizer53 = new wxGridSizer( 2, 3, 0, 0 );
	
	wxBoxSizer* bSizer244;
	bSizer244 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText94 = new wxStaticText( m_panel2112, wxID_ANY, wxT("size"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText94->Wrap( -1 );
	bSizer244->Add( m_staticText94, 0, wxALIGN_CENTER|wxALL, 5 );
	
	wxBoxSizer* bSizer254;
	bSizer254 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer265;
	bSizer265 = new wxBoxSizer( wxHORIZONTAL );
	
	graySizeHorizontal = new wxTextCtrl( m_panel2112, wxID_ANY, wxT("50"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer265->Add( graySizeHorizontal, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_staticText105 = new wxStaticText( m_panel2112, wxID_ANY, wxT("horizontal"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText105->Wrap( -1 );
	bSizer265->Add( m_staticText105, 0, wxALIGN_CENTER|wxALL, 5 );
	
	bSizer254->Add( bSizer265, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer2614;
	bSizer2614 = new wxBoxSizer( wxHORIZONTAL );
	
	graySizeVertical = new wxTextCtrl( m_panel2112, wxID_ANY, wxT("50"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer2614->Add( graySizeVertical, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_staticText1014 = new wxStaticText( m_panel2112, wxID_ANY, wxT("vertical"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1014->Wrap( -1 );
	bSizer2614->Add( m_staticText1014, 0, wxALIGN_CENTER|wxALL, 5 );
	
	bSizer254->Add( bSizer2614, 1, wxEXPAND, 5 );
	
	bSizer244->Add( bSizer254, 1, wxEXPAND, 5 );
	
	gSizer53->Add( bSizer244, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer2423;
	bSizer2423 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText923 = new wxStaticText( m_panel2112, wxID_ANY, wxT("step"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText923->Wrap( -1 );
	bSizer2423->Add( m_staticText923, 0, wxALIGN_CENTER|wxALL, 5 );
	
	wxBoxSizer* bSizer2523;
	bSizer2523 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer2633;
	bSizer2633 = new wxBoxSizer( wxHORIZONTAL );
	
	grayStepHorizontal = new wxTextCtrl( m_panel2112, wxID_ANY, wxT("20"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer2633->Add( grayStepHorizontal, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_staticText1033 = new wxStaticText( m_panel2112, wxID_ANY, wxT("horizontal"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1033->Wrap( -1 );
	bSizer2633->Add( m_staticText1033, 0, wxALIGN_CENTER|wxALL, 5 );
	
	bSizer2523->Add( bSizer2633, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer26123;
	bSizer26123 = new wxBoxSizer( wxHORIZONTAL );
	
	grayStepVertical = new wxTextCtrl( m_panel2112, wxID_ANY, wxT("20"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer26123->Add( grayStepVertical, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_staticText10123 = new wxStaticText( m_panel2112, wxID_ANY, wxT("vertical"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText10123->Wrap( -1 );
	bSizer26123->Add( m_staticText10123, 0, wxALIGN_CENTER|wxALL, 5 );
	
	bSizer2523->Add( bSizer26123, 1, wxEXPAND, 5 );
	
	bSizer2423->Add( bSizer2523, 1, wxEXPAND, 5 );
	
	gSizer53->Add( bSizer2423, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer11211;
	bSizer11211 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText6111 = new wxStaticText( m_panel2112, wxID_ANY, wxT("threshold"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText6111->Wrap( -1 );
	bSizer11211->Add( m_staticText6111, 0, wxALIGN_CENTER|wxALL, 5 );
	
	grayThreshold = new wxTextCtrl( m_panel2112, wxID_ANY, wxT("0.5"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer11211->Add( grayThreshold, 0, wxALIGN_CENTER|wxALL, 5 );
	
	gSizer53->Add( bSizer11211, 1, wxEXPAND, 5 );
	
	m_panel2112->SetSizer( gSizer53 );
	m_panel2112->Layout();
	gSizer53->Fit( m_panel2112 );
	bSizer6112->Add( m_panel2112, 1, wxEXPAND | wxALL, 5 );
	
	m_panel14->SetSizer( bSizer6112 );
	m_panel14->Layout();
	bSizer6112->Fit( m_panel14 );
	m_notebook2->AddPage( m_panel14, wxT("Gray filter"), false );
	m_panel15 = new wxPanel( m_notebook2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel15->Hide();
	
	m_notebook2->AddPage( m_panel15, wxT("Mask"), false );
	m_panel16 = new wxPanel( m_notebook2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer6113;
	bSizer6113 = new wxBoxSizer( wxHORIZONTAL );
	
	m_panel20113 = new wxPanel( m_panel16, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer713;
	bSizer713 = new wxBoxSizer( wxVERTICAL );
	
	deskewDisable = new wxRadioButton( m_panel20113, wxID_ANY, wxT("disable"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer713->Add( deskewDisable, 0, wxALL, 5 );
	
	deskewManual = new wxRadioButton( m_panel20113, wxID_ANY, wxT("manual"), wxDefaultPosition, wxDefaultSize, 0 );
	deskewManual->SetValue( true ); 
	bSizer713->Add( deskewManual, 0, wxALL, 5 );
	
	m_panel20113->SetSizer( bSizer713 );
	m_panel20113->Layout();
	bSizer713->Fit( m_panel20113 );
	bSizer6113->Add( m_panel20113, 0, wxEXPAND | wxALL, 5 );
	
	m_panel2113 = new wxPanel( m_panel16, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxGridSizer* gSizer54;
	gSizer54 = new wxGridSizer( 2, 3, 0, 0 );
	
	wxBoxSizer* bSizer1124;
	bSizer1124 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText614 = new wxStaticText( m_panel2113, wxID_ANY, wxT("scan size"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText614->Wrap( -1 );
	bSizer1124->Add( m_staticText614, 0, wxALIGN_CENTER|wxALL, 5 );
	
	deskewScanSize = new wxTextCtrl( m_panel2113, wxID_ANY, wxT("1500"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer1124->Add( deskewScanSize, 0, wxALIGN_CENTER|wxALL, 5 );
	
	gSizer54->Add( bSizer1124, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer212;
	bSizer212 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText82 = new wxStaticText( m_panel2113, wxID_ANY, wxT("scan direction"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText82->Wrap( -1 );
	bSizer212->Add( m_staticText82, 0, wxALIGN_CENTER|wxALL, 5 );
	
	wxBoxSizer* bSizer98;
	bSizer98 = new wxBoxSizer( wxVERTICAL );
	
	deskewScanLeft = new wxCheckBox( m_panel2113, wxID_ANY, wxT("left"), wxDefaultPosition, wxDefaultSize, 0 );
	deskewScanLeft->SetValue(true);
	
	bSizer98->Add( deskewScanLeft, 0, wxALIGN_CENTER|wxALL, 5 );
	
	deskewScanRight = new wxCheckBox( m_panel2113, wxID_ANY, wxT("right"), wxDefaultPosition, wxDefaultSize, 0 );
	deskewScanRight->SetValue(true);
	
	bSizer98->Add( deskewScanRight, 0, wxALIGN_CENTER|wxALL, 5 );
	
	bSizer212->Add( bSizer98, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer981;
	bSizer981 = new wxBoxSizer( wxVERTICAL );
	
	deskewScanTop = new wxCheckBox( m_panel2113, wxID_ANY, wxT("top"), wxDefaultPosition, wxDefaultSize, 0 );
	
	bSizer981->Add( deskewScanTop, 0, wxALIGN_CENTER|wxALL, 5 );
	
	deskewScanBottom = new wxCheckBox( m_panel2113, wxID_ANY, wxT("bottom"), wxDefaultPosition, wxDefaultSize, 0 );
	
	bSizer981->Add( deskewScanBottom, 0, wxALIGN_CENTER|wxALL, 5 );
	
	bSizer212->Add( bSizer981, 1, wxEXPAND, 5 );
	
	gSizer54->Add( bSizer212, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer11212;
	bSizer11212 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText6112 = new wxStaticText( m_panel2113, wxID_ANY, wxT("scan depth"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText6112->Wrap( -1 );
	bSizer11212->Add( m_staticText6112, 0, wxALIGN_CENTER|wxALL, 5 );
	
	deskewScanDepth = new wxTextCtrl( m_panel2113, wxID_ANY, wxT("0.5"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer11212->Add( deskewScanDepth, 0, wxALIGN_CENTER|wxALL, 5 );
	
	gSizer54->Add( bSizer11212, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer112121;
	bSizer112121 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText61121 = new wxStaticText( m_panel2113, wxID_ANY, wxT("scan range"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText61121->Wrap( -1 );
	bSizer112121->Add( m_staticText61121, 0, wxALIGN_CENTER|wxALL, 5 );
	
	deskewScanRange = new wxTextCtrl( m_panel2113, wxID_ANY, wxT("5.0"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer112121->Add( deskewScanRange, 0, wxALIGN_CENTER|wxALL, 5 );
	
	gSizer54->Add( bSizer112121, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer112122;
	bSizer112122 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText61122 = new wxStaticText( m_panel2113, wxID_ANY, wxT("scan step"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText61122->Wrap( -1 );
	bSizer112122->Add( m_staticText61122, 0, wxALIGN_CENTER|wxALL, 5 );
	
	deskewScanStep = new wxTextCtrl( m_panel2113, wxID_ANY, wxT("0.1"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer112122->Add( deskewScanStep, 0, wxALIGN_CENTER|wxALL, 5 );
	
	gSizer54->Add( bSizer112122, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer112123;
	bSizer112123 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText61123 = new wxStaticText( m_panel2113, wxID_ANY, wxT("scan deviation"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText61123->Wrap( -1 );
	bSizer112123->Add( m_staticText61123, 0, wxALIGN_CENTER|wxALL, 5 );
	
	deskewScanDeviation = new wxTextCtrl( m_panel2113, wxID_ANY, wxT("1.0"), wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer112123->Add( deskewScanDeviation, 0, wxALIGN_CENTER|wxALL, 5 );
	
	gSizer54->Add( bSizer112123, 1, wxEXPAND, 5 );
	
	m_panel2113->SetSizer( gSizer54 );
	m_panel2113->Layout();
	gSizer54->Fit( m_panel2113 );
	bSizer6113->Add( m_panel2113, 1, wxEXPAND | wxALL, 5 );
	
	m_panel16->SetSizer( bSizer6113 );
	m_panel16->Layout();
	bSizer6113->Fit( m_panel16 );
	m_notebook2->AddPage( m_panel16, wxT("Deskew"), false );
	m_panel17 = new wxPanel( m_notebook2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel17->Hide();
	
	m_notebook2->AddPage( m_panel17, wxT("Border"), false );
	m_panel18 = new wxPanel( m_notebook2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel18->Hide();
	
	m_notebook2->AddPage( m_panel18, wxT("Brightness"), false );
	
	gSizer3->Add( m_notebook2, 1, wxEXPAND | wxALL, 5 );
	
	fgSizer1->Add( gSizer3, 0, wxEXPAND, 5 );
	
	this->SetSizer( fgSizer1 );
	this->Layout();
	m_menubar1 = new wxMenuBar( 0 );
	m_menu1 = new wxMenu();
	wxMenuItem* m_menuItem1;
	m_menuItem1 = new wxMenuItem( m_menu1, wxID_OPEN, wxString( wxT("&Open") ) , wxEmptyString, wxITEM_NORMAL );
	m_menu1->Append( m_menuItem1 );
	
	wxMenuItem* m_menuItem6;
	m_menuItem6 = new wxMenuItem( m_menu1, wxID_SAVE, wxString( wxT("&Save") ) , wxEmptyString, wxITEM_NORMAL );
	m_menu1->Append( m_menuItem6 );
	
	m_menu1->AppendSeparator();
	
	wxMenuItem* m_menuItem4;
	m_menuItem4 = new wxMenuItem( m_menu1, wxID_ANY, wxString( wxT("&Load unpaper settings...") ) , wxEmptyString, wxITEM_NORMAL );
	m_menu1->Append( m_menuItem4 );
	
	wxMenuItem* m_menuItem5;
	m_menuItem5 = new wxMenuItem( m_menu1, wxID_ANY, wxString( wxT("S&ave unpaper settings...") ) , wxEmptyString, wxITEM_NORMAL );
	m_menu1->Append( m_menuItem5 );
	
	m_menubar1->Append( m_menu1, wxT("&File") );
	
	m_menu3 = new wxMenu();
	wxMenuItem* m_menuItem31;
	m_menuItem31 = new wxMenuItem( m_menu3, wxID_COPY, wxString( wxT("Copy unpaper args to clipboard") ) , wxEmptyString, wxITEM_NORMAL );
	m_menu3->Append( m_menuItem31 );
	
	m_menubar1->Append( m_menu3, wxT("Edit") );
	
	m_menu2 = new wxMenu();
	wxMenuItem* m_menuItem3;
	m_menuItem3 = new wxMenuItem( m_menu2, wxID_ABOUT, wxString( wxT("&About") ) , wxEmptyString, wxITEM_NORMAL );
	m_menu2->Append( m_menuItem3 );
	
	m_menubar1->Append( m_menu2, wxT("&Help") );
	
	this->SetMenuBar( m_menubar1 );
	
	
	// Connect Events
	m_button2->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( UnpaperGUIBase::preview1Handler ), NULL, this );
	m_button21->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( UnpaperGUIBase::preview2Handler ), NULL, this );
	scaleCombo->Connect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( UnpaperGUIBase::scaleHandler ), NULL, this );
	scaleCombo->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::scaleHandler ), NULL, this );
	m_button3->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( UnpaperGUIBase::clearLogHandler ), NULL, this );
	whiteTreshold->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::generalHandler ), NULL, this );
	blackTreshold->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::generalHandler ), NULL, this );
	layoutSingle->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( UnpaperGUIBase::generalHandler ), NULL, this );
	layoutDouble->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( UnpaperGUIBase::generalHandler ), NULL, this );
	layoutNone->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( UnpaperGUIBase::generalHandler ), NULL, this );
	preRotate->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( UnpaperGUIBase::preprocessHandler ), NULL, this );
	preMirrorHorizontal->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( UnpaperGUIBase::preprocessHandler ), NULL, this );
	preMirrorVertical->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( UnpaperGUIBase::preprocessHandler ), NULL, this );
	preShiftHorizontal->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::preprocessHandler ), NULL, this );
	preShiftVertical->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::preprocessHandler ), NULL, this );
	sizeChoice->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( UnpaperGUIBase::preprocessHandler ), NULL, this );
	landscapeCheck->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( UnpaperGUIBase::preprocessHandler ), NULL, this );
	postRotate->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( UnpaperGUIBase::postprocessHandler ), NULL, this );
	postMirrorHorizontal->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( UnpaperGUIBase::postprocessHandler ), NULL, this );
	postMirrorVertical->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( UnpaperGUIBase::postprocessHandler ), NULL, this );
	postShiftHorizontal->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::postprocessHandler ), NULL, this );
	postShiftVertical->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::postprocessHandler ), NULL, this );
	postSize->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( UnpaperGUIBase::postprocessHandler ), NULL, this );
	postLandscapeCheck->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( UnpaperGUIBase::postprocessHandler ), NULL, this );
	noiseDisable->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( UnpaperGUIBase::noiseHandler ), NULL, this );
	noiseManual->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( UnpaperGUIBase::noiseHandler ), NULL, this );
	noiseIntensity->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::noiseHandler ), NULL, this );
	blackDisable->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( UnpaperGUIBase::blackHandler ), NULL, this );
	blackManual->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( UnpaperGUIBase::blackHandler ), NULL, this );
	blackIntensity->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::blackHandler ), NULL, this );
	blackScanHorizontal->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( UnpaperGUIBase::blackHandler ), NULL, this );
	blackScanVertical->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( UnpaperGUIBase::blackHandler ), NULL, this );
	blackScanSizeHorizontal->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::blackHandler ), NULL, this );
	blackScanSizeVertical->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::blackHandler ), NULL, this );
	blackScanDepthHorizontal->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::blackHandler ), NULL, this );
	blackScanDepthVertical->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::blackHandler ), NULL, this );
	blackScanStepHorizontal->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::blackHandler ), NULL, this );
	blackScanStepVertical->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::blackHandler ), NULL, this );
	blackScanThreshold->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::blackHandler ), NULL, this );
	blurDisable->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( UnpaperGUIBase::blurHandler ), NULL, this );
	blurManual->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( UnpaperGUIBase::blurHandler ), NULL, this );
	blurIntensity->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::blurHandler ), NULL, this );
	blurSizeHorizontal->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::blurHandler ), NULL, this );
	blurSizeVertical->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::blurHandler ), NULL, this );
	blurStepHorizontal->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::blurHandler ), NULL, this );
	blurStepVertical->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::blurHandler ), NULL, this );
	grayDisable->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( UnpaperGUIBase::grayHandler ), NULL, this );
	grayManual->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( UnpaperGUIBase::grayHandler ), NULL, this );
	graySizeHorizontal->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::grayHandler ), NULL, this );
	graySizeVertical->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::grayHandler ), NULL, this );
	grayStepHorizontal->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::grayHandler ), NULL, this );
	grayStepVertical->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::grayHandler ), NULL, this );
	grayThreshold->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::grayHandler ), NULL, this );
	deskewDisable->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( UnpaperGUIBase::deskewHandler ), NULL, this );
	deskewManual->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( UnpaperGUIBase::deskewHandler ), NULL, this );
	deskewScanSize->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::deskewHandler ), NULL, this );
	deskewScanLeft->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( UnpaperGUIBase::deskewHandler ), NULL, this );
	deskewScanRight->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( UnpaperGUIBase::deskewHandler ), NULL, this );
	deskewScanTop->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( UnpaperGUIBase::deskewHandler ), NULL, this );
	deskewScanBottom->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( UnpaperGUIBase::deskewHandler ), NULL, this );
	deskewScanDepth->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::deskewHandler ), NULL, this );
	deskewScanRange->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::deskewHandler ), NULL, this );
	deskewScanStep->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::deskewHandler ), NULL, this );
	deskewScanDeviation->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::deskewHandler ), NULL, this );
	this->Connect( m_menuItem1->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( UnpaperGUIBase::openHandler ) );
	this->Connect( m_menuItem6->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( UnpaperGUIBase::saveHandler ) );
	this->Connect( m_menuItem4->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( UnpaperGUIBase::loadSettingsHandler ) );
	this->Connect( m_menuItem5->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( UnpaperGUIBase::saveSettingsHandler ) );
	this->Connect( m_menuItem31->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( UnpaperGUIBase::copyArgsToClipboardHandler ) );
	this->Connect( m_menuItem3->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( UnpaperGUIBase::aboutHandler ) );
}

UnpaperGUIBase::~UnpaperGUIBase()
{
	// Disconnect Events
	m_button2->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( UnpaperGUIBase::preview1Handler ), NULL, this );
	m_button21->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( UnpaperGUIBase::preview2Handler ), NULL, this );
	scaleCombo->Disconnect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( UnpaperGUIBase::scaleHandler ), NULL, this );
	scaleCombo->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::scaleHandler ), NULL, this );
	m_button3->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( UnpaperGUIBase::clearLogHandler ), NULL, this );
	whiteTreshold->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::generalHandler ), NULL, this );
	blackTreshold->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::generalHandler ), NULL, this );
	layoutSingle->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( UnpaperGUIBase::generalHandler ), NULL, this );
	layoutDouble->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( UnpaperGUIBase::generalHandler ), NULL, this );
	layoutNone->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( UnpaperGUIBase::generalHandler ), NULL, this );
	preRotate->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( UnpaperGUIBase::preprocessHandler ), NULL, this );
	preMirrorHorizontal->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( UnpaperGUIBase::preprocessHandler ), NULL, this );
	preMirrorVertical->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( UnpaperGUIBase::preprocessHandler ), NULL, this );
	preShiftHorizontal->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::preprocessHandler ), NULL, this );
	preShiftVertical->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::preprocessHandler ), NULL, this );
	sizeChoice->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( UnpaperGUIBase::preprocessHandler ), NULL, this );
	landscapeCheck->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( UnpaperGUIBase::preprocessHandler ), NULL, this );
	postRotate->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( UnpaperGUIBase::postprocessHandler ), NULL, this );
	postMirrorHorizontal->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( UnpaperGUIBase::postprocessHandler ), NULL, this );
	postMirrorVertical->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( UnpaperGUIBase::postprocessHandler ), NULL, this );
	postShiftHorizontal->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::postprocessHandler ), NULL, this );
	postShiftVertical->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::postprocessHandler ), NULL, this );
	postSize->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( UnpaperGUIBase::postprocessHandler ), NULL, this );
	postLandscapeCheck->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( UnpaperGUIBase::postprocessHandler ), NULL, this );
	noiseDisable->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( UnpaperGUIBase::noiseHandler ), NULL, this );
	noiseManual->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( UnpaperGUIBase::noiseHandler ), NULL, this );
	noiseIntensity->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::noiseHandler ), NULL, this );
	blackDisable->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( UnpaperGUIBase::blackHandler ), NULL, this );
	blackManual->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( UnpaperGUIBase::blackHandler ), NULL, this );
	blackIntensity->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::blackHandler ), NULL, this );
	blackScanHorizontal->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( UnpaperGUIBase::blackHandler ), NULL, this );
	blackScanVertical->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( UnpaperGUIBase::blackHandler ), NULL, this );
	blackScanSizeHorizontal->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::blackHandler ), NULL, this );
	blackScanSizeVertical->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::blackHandler ), NULL, this );
	blackScanDepthHorizontal->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::blackHandler ), NULL, this );
	blackScanDepthVertical->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::blackHandler ), NULL, this );
	blackScanStepHorizontal->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::blackHandler ), NULL, this );
	blackScanStepVertical->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::blackHandler ), NULL, this );
	blackScanThreshold->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::blackHandler ), NULL, this );
	blurDisable->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( UnpaperGUIBase::blurHandler ), NULL, this );
	blurManual->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( UnpaperGUIBase::blurHandler ), NULL, this );
	blurIntensity->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::blurHandler ), NULL, this );
	blurSizeHorizontal->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::blurHandler ), NULL, this );
	blurSizeVertical->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::blurHandler ), NULL, this );
	blurStepHorizontal->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::blurHandler ), NULL, this );
	blurStepVertical->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::blurHandler ), NULL, this );
	grayDisable->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( UnpaperGUIBase::grayHandler ), NULL, this );
	grayManual->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( UnpaperGUIBase::grayHandler ), NULL, this );
	graySizeHorizontal->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::grayHandler ), NULL, this );
	graySizeVertical->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::grayHandler ), NULL, this );
	grayStepHorizontal->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::grayHandler ), NULL, this );
	grayStepVertical->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::grayHandler ), NULL, this );
	grayThreshold->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::grayHandler ), NULL, this );
	deskewDisable->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( UnpaperGUIBase::deskewHandler ), NULL, this );
	deskewManual->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( UnpaperGUIBase::deskewHandler ), NULL, this );
	deskewScanSize->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::deskewHandler ), NULL, this );
	deskewScanLeft->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( UnpaperGUIBase::deskewHandler ), NULL, this );
	deskewScanRight->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( UnpaperGUIBase::deskewHandler ), NULL, this );
	deskewScanTop->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( UnpaperGUIBase::deskewHandler ), NULL, this );
	deskewScanBottom->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( UnpaperGUIBase::deskewHandler ), NULL, this );
	deskewScanDepth->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::deskewHandler ), NULL, this );
	deskewScanRange->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::deskewHandler ), NULL, this );
	deskewScanStep->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::deskewHandler ), NULL, this );
	deskewScanDeviation->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( UnpaperGUIBase::deskewHandler ), NULL, this );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( UnpaperGUIBase::openHandler ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( UnpaperGUIBase::saveHandler ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( UnpaperGUIBase::loadSettingsHandler ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( UnpaperGUIBase::saveSettingsHandler ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( UnpaperGUIBase::copyArgsToClipboardHandler ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( UnpaperGUIBase::aboutHandler ) );
}
