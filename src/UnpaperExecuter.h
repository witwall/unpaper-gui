#ifndef _UNPAPEREXECUTER_H_
#define _UNPAPEREXECUTER_H_

#include <wx/wx.h>

#include "Executer.h"
#include "Options.h"

class UnpaperExecuter : public Executer
{
	private:
		wxString cmd;
		virtual wxString *getCmd();

	public:
		UnpaperExecuter();
		
		long unpaper(wxString from, wxString to, Options *opts, wxTextCtrl *log);
};

#endif
